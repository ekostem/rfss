OS = $(shell uname -s)
CXX = icpc -static -static-intel
CFLAGS = -O3  -Wall -vec-report1
LIBDIR = -L$(MKLROOT)/lib/intel64
INCLUDE = -I$(MKLROOT)/include

ifeq ($(OS),Darwin)
	LIBS = -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lpthread
endif

ifeq ($(OS),Linux)
#	LIBS = -lmkl_solver_lp64 -Wl,--start-group -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -Wl,--end-group -liomp5 -lpthread
	LIBS = -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread
endif


OBJECTS = Utility.o InputFile.o Population.o SNP.o LDBlock.o GWAS.o SimulateGWAS.o ReadGWAS.o AnalyzeGWAS.o RFSS.o TraditionalSNPSelection.o main.o

WTCCC = Utility.o InputFile.o Population.o SNP.o LDBlock.o GWAS.o SimulateGWAS.o ReadGWAS.o AnalyzeGWAS.o RFSS.o TraditionalSNPSelection.o wtccc.o

#RFSS = Utility.o InputFile.o Population.o SNP.o LDBlock.o GWAS.o SimulateGWAS.o ReadGWAS.o AnalyzeGWAS.o RFSS.o TraditionalSNPSelection.o rfss_main.o

RFSS = Utility.o InputFile.o Population.o SNP.o LDBlock.o GWAS.o SimulateGWAS.o ReadGWAS.o AnalyzeGWAS.o RFSS.o TraditionalSNPSelection.o main.o



TEST = Utility.o


all : $(RFSS)
	$(CXX) $(CFLAGS) $(RFSS) $(INCLUDE) $(LIBDIR) $(LIBS) -o rfss


wtccc : $(WTCCC)
	$(CXX) $(CFLAGS) $(WTCCC) $(INCLUDE) $(LIBDIR) $(LIBS) -o wtccc

rfss_main.o : ./src/rfss_main.cpp
	$(CXX) $(CFLAGS) -c ./src/rfss_main.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


wtccc.o : ./src/wtccc.cpp
	$(CXX) $(CFLAGS) -c ./src/wtccc.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

main.o : ./src/main.cpp
	$(CXX) $(CFLAGS) -c ./src/main.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

TraditionalSNPSelection.o : ./src/TraditionalSNPSelection.cpp
	$(CXX) $(CFLAGS) -c ./src/TraditionalSNPSelection.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

RFSS.o : ./src/RFSS.cpp
	$(CXX) $(CFLAGS) -c ./src/RFSS.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

AnalyzeGWAS.o : ./src/AnalyzeGWAS.cpp
	$(CXX) $(CFLAGS) -c ./src/AnalyzeGWAS.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

ReadGWAS.o : ./src/ReadGWAS.cpp
	$(CXX) $(CFLAGS) -c ./src/ReadGWAS.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

SimulateGWAS.o : ./src/SimulateGWAS.cpp
	$(CXX) $(CFLAGS) -c ./src/SimulateGWAS.cpp $(INCLUDE) $(LIBDIR) $(LIBS)
	
GWAS.o : ./src/GWAS.cpp
	$(CXX) $(CFLAGS) -c ./src/GWAS.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

Population.o : ./src/Population.cpp
	$(CXX) $(CFLAGS) -c ./src/Population.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

LDBlock.o : ./src/LDBlock.cpp
	$(CXX) $(CFLAGS) -c ./src/LDBlock.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

SNP.o : ./src/SNP.cpp
	$(CXX) $(CFLAGS) -c ./src/SNP.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

InputFile.o : ./src/InputFile.cpp
	$(CXX) $(CFLAGS) -c ./src/InputFile.cpp $(INCLUDE) $(LIBDIR) $(LIBS)
	
Utility.o : ./src/Utility.cpp
	$(CXX) $(CFLAGS) -c ./src/Utility.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


clean:
	@echo $(OS)
	@echo $(LIBS)
	rm *.o
