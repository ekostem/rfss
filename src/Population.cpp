/*
 *  Population.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/21/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */

#include "../include/Population.h"



const string Population::emptyKey = "EMPTYKEY";
const string Population::selfKey = "SELFKEY";



Population::~Population( )
{
	correlationHashTable.clear();
	MAFHashTable.clear();
	positionHashTable.clear();
	chromosomeHashTable.clear();
	rsIDSet.clear();
}


Population::Population()
{
	populationName = "NA";
	correlationHashTable.insert( make_pair( emptyKey, 0.0  ) );
	correlationHashTable.insert( make_pair( selfKey, 1.0  ) );
	MAFHashTable.insert( make_pair( emptyKey, 0.0  ) );
	
}


Population::Population(const string& popName = string(""), 
											 const string& popCorrelationFilename = string(""), 
											 const string& popMAFFilename = string(""),
											 const string& popMAPFilename = string("")
											 )
{
	populationName = popName;
	correlationHashTable.insert( make_pair( emptyKey, 0.0  ) );
	correlationHashTable.insert( make_pair( selfKey, 1.0  ) );
	MAFHashTable.insert( make_pair( emptyKey, 0.0  ) );
	
	// read the file and fill the correlation information
	InputFile* correlationFile = NULL;
	try
	{
		correlationFile = new InputFile(popCorrelationFilename);
		for (long line=0; line < correlationFile->length(); line++)
		{
			
			setCorrelation(correlationFile->at(line)->at(0), 
										 correlationFile->at(line)->at(1), 
										 strtod( correlationFile->at(line)->at(2).c_str(), NULL)
										 );
		}
	}
	catch (InputFilexception& e)
	{
		cerr << e.what() << endl;
	}
	if (correlationFile) 
	{
		delete correlationFile;
	}
	
	InputFile* mafFile = NULL;
	try
	{
		mafFile = new InputFile(popMAFFilename);
		
		for (long line=0; line < mafFile->length(); line++)
		{
			setMAF(mafFile->at(line)->at(0), 
						 strtod( mafFile->at(line)->at(1).c_str(), NULL)
						 );
		}
		
	}
	catch (InputFilexception& e)
	{
		cerr << e.what() << endl;
	}

	if (mafFile) 
	{
		delete mafFile;
	}

	
	/********************************************
	 rsID chrom position
	 
	*********************************************/
	InputFile* mapFile = NULL;
	try
	{
		mapFile = new InputFile(popMAPFilename);
		
		for (long line=0; line < mapFile->length(); line++)
		{

			rsIDSet.insert( mapFile->at(line)->at(0) );
			
			setChromosome(mapFile->at(line)->at(0), 
									mapFile->at(line)->at(1)
									);
			
			
			setPosition(mapFile->at(line)->at(0), 
						 atol( mapFile->at(line)->at(2).c_str() )
						 );
		}
	}
	catch (InputFilexception& e)
	{
		cerr << e.what() << endl;
	}
	if (mapFile) 
	{
		delete mapFile;
	}
}


Population::Population(const Population& source)
{
	populationName = source.populationName;
	populationInfo = source.populationInfo;
	
	correlationHashTable.clear();
	correlationHashTable = source.correlationHashTable;
	
	MAFHashTable.clear();
	MAFHashTable = source.MAFHashTable;
	
	chromosomeHashTable.clear();
	chromosomeHashTable = source.chromosomeHashTable;
	
	rsIDSet.clear();
	rsIDSet = source.rsIDSet;
}


Population& Population::operator=(const Population& source)
{
	populationName = source.populationName;
	populationInfo = source.populationInfo;
	
	correlationHashTable.clear();
	correlationHashTable = source.correlationHashTable;
	
	MAFHashTable.clear();
	MAFHashTable = source.MAFHashTable;
	
	chromosomeHashTable.clear();
	chromosomeHashTable = source.chromosomeHashTable;
	
	rsIDSet.clear();
	rsIDSet = source.rsIDSet;
	
	
	return *this;
}

inline string Population::getCorrelationKey( const string& snp1, const string& snp2) const
{
	if ( 	snp1.size() && snp2.size() )
	{
		if ( snp1 == snp2 )
		{
			return selfKey;
		}
		else if ( snp1 < snp2 )
		{
			return (snp1 + "_" + snp2);
		}
		else 
		{
			return (snp2 + "_" + snp1);
		}
	}
	else
	{
		return emptyKey; // return empty string
	}		
}


string Population::getCorrelationKey( const SNP* snp1, const SNP* snp2) const
{
	if ( 	snp1 && snp2 )
	{
		return getCorrelationKey(snp1->getrsID(), snp1->getrsID());
	}
	else
	{
		return emptyKey; // return empty string
	}		
}



void Population::getCorrelation(const string& snp1, const string& snp2, double& retval) const
{
	string key( getCorrelationKey(snp1, snp2) );
	correlationHash::const_iterator it;
	it = correlationHashTable.find( key );
	if ( it != correlationHashTable.end() ) 
	{
		retval = it->second;
	}
	else 
	{
		retval = 0.0;
	}
}


void Population::getCorrelation(const SNP* snp1, const SNP* snp2, double& retval) const
{
	getCorrelation(snp1->getrsID(), snp2->getrsID(), retval);
}


void Population::getCorrelation(const SNP* snp1, const vector<const SNP*>& snps, vector<double>& retval) const
{
	retval.clear();
	retval.resize(snps.size(), 0.0 );
	int i = 0;
	for ( vector<const SNP*>::const_iterator it = snps.begin(); it != snps.end(); it++, i++) 
	{
		getCorrelation(snp1, *it, retval.at(i) );
	}
}


void Population::getCorrelation(const vector<const SNP*>& snps1, const vector<const SNP*>& snps2, vector<double>& retval) const
{
	
	int nrow = snps1.size();
	int ncol = snps2.size();
	retval.clear();
	retval.resize( nrow*ncol, 0.0 );
	
	//We'll store in column format (each column is stored in the order)
	int col = 0;
	int row = 0;
	for ( vector<const SNP*>::const_iterator col_it = snps2.begin(); col_it != snps2.end(); col_it++, col++)
	{
		row = 0;
		for ( vector<const SNP*>::const_iterator row_it = snps1.begin(); row_it != snps1.end(); row_it++, row++)	
		{
			getCorrelation(*col_it, *row_it, retval.at(  col*nrow + row ) );
		}
	}
}


void Population::setCorrelation(const string& snp1, const string& snp2, const double& value)
{
	string key( getCorrelationKey(snp1, snp2) );
	if ( key != selfKey && key != emptyKey)
	{
		correlationHashTable.erase( key );	
		correlationHashTable.insert( make_pair( key, value) );
	}
}


void Population::setCorrelation(const SNP* snp1, const SNP* snp2, const double& value)
{
	setCorrelation(snp1->getrsID(), snp2->getrsID(), value);
}


void Population::setMAF(const string& rsid, const double& maf)
{
	if ( rsid != selfKey && rsid != emptyKey )
	{
		MAFHashTable.erase( rsid );
		MAFHashTable.insert( make_pair( rsid, maf) );
	}
}


void Population::getMAF(const vector<const SNP*>& snps, vector<double>& mafs) const
{
	vector<const SNP*>::const_iterator it;
	mafs.clear();
	mafs.resize( snps.size() );
	int i = 0;
	for (it = snps.begin(); it != snps.end(); it++, i++)
	{
		getMAF(*it, mafs.at(i));
	}
}



void Population::getMAF(const string& rsid, double& maf) const
{
	MAFHash::const_iterator it;
	it = MAFHashTable.find( rsid );
	if ( it != MAFHashTable.end() ) 
	{
		maf = it->second;
	}
	else 
	{
		maf = 0.0;	
	}
}


void Population::getMAF(const SNP* snp, double& maf) const
{
	getMAF(snp->getrsID(), maf);
}


void Population::setPosition(const string& rsid, const long& position)
{
	if ( rsid != selfKey && rsid != emptyKey )
	{
		positionHashTable.erase( rsid );
		positionHashTable.insert( make_pair( rsid, position) );
	}
}


void Population::getPosition(const string& rsid, long& position) const
{
	positionHash::const_iterator it;
	it = positionHashTable.find( rsid );
	if ( it != positionHashTable.end() ) 
	{
		position = it->second;
	}
	else 
	{
		position = 0;	
	}
}


void Population::getPosition(const SNP* snp, long& position) const
{
	getPosition(snp->getrsID(), position);
}


void Population::getPosition(const vector<const SNP*>& snps, vector<long>& retval) const
{
	retval.resize( snps.size() );
	int i = 0;
	for ( vector<const SNP*>::const_iterator it = snps.begin(); it != snps.end(); it++, i++) 
	{
		getPosition(*it, retval.at(i) );
	}
}


void Population::setChromosome(const string& rsid, const string& chrom)
{
	if ( rsid != selfKey && rsid != emptyKey )
	{
		chromosomeHashTable.erase( rsid );
		chromosomeHashTable.insert( make_pair( rsid, chrom) );
	}
}


void Population::getChromosome(const string& rsid, string& chrom) const
{
	chromosomeHash::const_iterator it;
	it = chromosomeHashTable.find( rsid );
	if ( it != chromosomeHashTable.end() ) 
	{
		chrom = it->second;
	}
	else 
	{
		chrom = "UNKNOWN";	
	}
}


bool Population::isSNPIncluded( const string& rsID)
{
	return (rsIDSet.find( rsID ) == rsIDSet.end() ) ? false : true;	
}


bool Population::isSNPIncluded( const SNP* snp)
{
	return isSNPIncluded( snp->getrsID() );
}





void Population::dumpCorrelationHash()
{
	cout << "Correlation Hash Table Size: " << correlationHashTable.size() << endl;
	correlationHash::iterator it;
	for (it = correlationHashTable.begin(); it != correlationHashTable.end(); it++)
	{
		cout << it->first << ": " << it->second << endl;	
	}
	cout << endl;
}










