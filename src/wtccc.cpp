/*
 *  wtccc.cpp
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 12/5/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */


#include "../include/TraditionalSNPSelection.h"
#include "../include/RFSS.h"
#include "../include/GWAS.h"
#include "../include/ReadGWAS.h"
#include <iostream>
#include <unistd.h>

using namespace std;

void print_help(void);

void print_help(void)
{
	cerr << "Usage: rfss [-p path] [-f prefixName] [-t #tagSNPs] [-n #candidateSNPs] [-o outputFile]" << endl;
	
	
	
}




int main(int argc, char* argv[])
{
	if (argc < 2 )
	{
		cerr << "Usage: study method chromosome" << endl;	
		cerr << "Method: {Corr, Dist, RFSS, RFSS_ALL}" << endl;
		return -1;
	}
	
	string path = "/var/cluster/scratch/ekostem/WTCCC/Data/";
	string outpath = "/var/cluster/scratch/ekostem/WTCCC/Results/";
	string study = argv[1];
	string method = argv[2];
	
	string chromosome = "";
	if ( argc == 4 )
	{
		chromosome = "_" + string(argv[3]);
	}
	
	WTCCC* T2D = NULL;
	GWAS* gwas = NULL;
	Population* CEU = NULL;
	
	RFSS* rfss = NULL;
	CorrelationApproach* Corr = NULL;
	
	CEU = new Population("CEU",
											 path + "WTCCC_Correlations" + chromosome + ".txt",
											 path + "WTCCC_MAF" + chromosome + ".txt",
											 path + "WTCCC_MAP" + chromosome + ".txt"
											 );		


	
	
	T2D = new WTCCC();
	T2D->registerPopulation(*CEU);
	T2D->setGwasFilename(path + "WTCCC_tag_" + study + ".txt");
	T2D->setGwasCandidateSNPFilename(path + "WTCCC_candidate_" + study + ".txt");
	T2D->readData();
	
	gwas = new GWAS();
	gwas->readAllocation(*T2D);
	gwas->readData(*T2D);
	
	if (method == "Corr")
	{
		Corr = new CorrelationApproach();
		Corr->setSignificanceLevel(1e-8);
		Corr->usePopulation(*CEU);
		gwas->writeData(*Corr);
		
		Corr->setCorrelationThreshold(0.9);
		Corr->correlationSelection();
		Corr->dumpResults(outpath + "Corr_09_" + study + ".txt");		
		
		Corr->setCorrelationThreshold(0.5);
		Corr->correlationSelection();
		Corr->dumpResults(outpath + "Corr_05_" + study + ".txt");		

		
		Corr->setCorrelationThreshold(0.1);
		Corr->correlationSelection();
		Corr->dumpResults(outpath + "Corr_01_" + study + ".txt");		
		
	}

	
	if (method == "RFSS")
	{
		rfss = new RFSS();
		rfss->setCausalSNPProbability(1e-6);
		rfss->setSignificanceLevel(1e-8);
		rfss->setCausalSNPPower(0.5);
		rfss->setTagCandidateDimension(1, 1);
		rfss->usePopulation(*CEU);
		
		gwas->writeData(*rfss);
		
		rfss->allTagSelection();
		rfss->dumpResults(outpath + "Rfss_best_" + study + ".txt");
	}

	if (method == "RFSS_ALL")
	{
		rfss = new RFSS();
		rfss->setCausalSNPProbability(1e-6);
		rfss->setSignificanceLevel(1e-8);
		rfss->setCausalSNPPower(0.5);
		rfss->setTagCandidateDimension(5, 10);
		rfss->usePopulation(*CEU);
		
		gwas->writeData(*rfss);
		
		rfss->allTagSelection();
		rfss->dumpResults(outpath + "Rfss_all_" + study + chromosome + ".txt");
	}
	
	
	
	if (method == "Corr")
		delete Corr;
	if (method == "RFSS" || method == "RFSS_ALL" )
		delete rfss;
	
	delete CEU;
	delete gwas;
	delete T2D;

	return 0;
}
