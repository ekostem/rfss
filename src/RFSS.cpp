/*
 *  RFSS.cpp
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 10/28/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#include "../include/RFSS.h"


RFSS::RFSS() : AnalyzeGWAS()
{
	causalSNPProbability = 1e-6;
	causalSNPPower = 0.5;
	significanceLevel = 1e-8;
	significanceThreshold = 5.723;
	causalSNPNCP = 5.723;
}

RFSS::~RFSS()
{
	local_followUpSNPs.clear();
}

void RFSS::setSignificanceLevel( double alpha )
{ 
	significanceLevel = alpha; 
	significanceThreshold = 1.0 - 0.5*significanceLevel;
	vdCdfNormInv(1, &significanceThreshold, &significanceThreshold);
}


double RFSS::powerFunction(double mean)
{
	double y[2];
	double a[2] = { -significanceThreshold - mean, significanceThreshold - mean};
	vdCdfNorm(2, a, y);
	return 1.0 - y[1] + y[0];
}


void RFSS::setCausalSNPPower( double power)
{
	/****************************************************************************
	 * Compute the Non-centrality Parameter corresponding to the User's Power
	 ****************************************************************************/
	
	double left = 0.0;
	double right = 10.0;
	double ncp = 5.0;
	double tol = 1e-10;
	
	/*********Binary Search for NCP***********************************************/
	while ( abs(powerFunction(ncp) - power) > tol )
	{
		if ( powerFunction(ncp) > power )
		{
			right = ncp;
			ncp = 0.5*( left + right );
		}
		else 
		{
			left = ncp;
			ncp = 0.5*( left + right );
		}
	}
	/****************************************************************************/
	causalSNPNCP = ncp;
}


void RFSS::dumpResults( const string& filename)
{
	
	if ( !local_followUpSNPs.size() )
	{
		return;
	}
	
	ofstream dumpFile;
	/******************************************************************************************
	 * Do the averaging for ties.
	 ******************************************************************************************/
	pair<vector<followUpSNP>::iterator,vector<followUpSNP>::iterator> bounds;
	vector<followUpSNP>::iterator it = local_followUpSNPs.begin();
	
	
	dumpFile.open( filename.c_str() );
	dumpFile << "k\tPPV\tLikelihood\tStat\tisCausal\tisAlt.\tPanelNo.\tCL\tNonCL\tStd\tCM\t\tNonCM\trsID\n";
	
	int k = 1;
	double sum = 0.0;
	double avg = 0.0;

	int len = 0;
	do
	{
		bounds = equal_range(it, local_followUpSNPs.end(), *it, sortSNPs() );
		len = int(bounds.second - bounds.first);
		avg = 0.0;
		for (vector<followUpSNP>::iterator pt = bounds.first; pt != bounds.second; pt++)
		{
			if ( abs(pt->candidateStatistic) > significanceThreshold )
			{
				avg++;
			}
		}
		avg = avg / double(len);
		for (vector<followUpSNP>::iterator pt = bounds.first; pt != bounds.second; pt++)
		{
			sum += avg;
			
			dumpFile	<< k << "\t"
			<< sum / double(k) << "\t"
			<< pt->likelihood << "\t"
			<< pt->candidateStatistic << "\t"
			<< pt->isCandidateCausal << "\t"
			<< pt->isAlternate << "\t"
			<< pt->panelNumber << "\t"
			<< pt->causalLikelihood << "\t"
			<< pt->noncausalLikelihood << "\t"
			<< pt->variance << "\t"
			<< pt->causalMean << "\t"
			<< pt->noncausalMean << "\t"
			<< pt->rsID << "\n";
			k++;
		}
		it = bounds.second;		
	} while ( it != local_followUpSNPs.end() );
	
	dumpFile.close();
}


void RFSS::dumpResults2( const string& filename)
{
	
	if ( !local_followUpSNPs.size() )
	{
		return;
	}
	
	ofstream dumpFile;
	
	dumpFile.open( filename.c_str() );
	dumpFile << "k\tPPV\tLikelihood\tStat\tisCausal\tisAlt.\tPanelNo.\tCL\tNonCL\tStd\tCM\t\tNonCM\trsID\n";
	
	int k = 1;
	double sum = 0.0;

	for (vector<followUpSNP>::iterator pt = local_followUpSNPs.begin(); pt != local_followUpSNPs.end(); pt++)
	{
		
		if (abs(pt->candidateStatistic) > significanceThreshold)
		{
			sum += 1;
		}
		
		dumpFile	<< k << "\t"
		<< sum / double(k) << "\t"
		<< pt->likelihood << "\t"
		<< pt->candidateStatistic << "\t"
		<< pt->isCandidateCausal << "\t"
		<< pt->isAlternate << "\t"
		<< pt->panelNumber << "\t"
		<< pt->causalLikelihood << "\t"
		<< pt->noncausalLikelihood << "\t"
		<< pt->variance << "\t"
		<< pt->causalMean << "\t"
		<< pt->noncausalMean << "\t"
		<< pt->rsID << "\n";
		k++;
	}
	
	dumpFile.close();
}


void RFSS::bestTagSelection()
{
	
	/******************************************************************************
	 * Get the tagSNPs in each LDBlock
	 * Store in a vector
	 * for each candidate get the vector of correlations, choose the max (best-tag)
	 * store in a vector, will do vector math.
	 * write in the class local array in pairs (likelihood, original statistic)
	 * terminate
	 ******************************************************************************/
	
	time_t timeStart, timeEnd;
	double timeDiff;
	time(&timeStart);
	
	//Get the start of the LDBlocks
	
	local_followUpSNPs.clear();
	
	vector<const SNP*> tags;
	vector<const SNP*> candidates;
	
	vector<double> sHat;
	vector<double> sHat_tags;
	vector<double> sHat_candidates;
	vector<double> R;
	vector<followUpSNP> followUpSNPs;
	vector<double> correlations;
	vector<double> candidateMAFs; // Store the MAFs of candidates
	
	bool isAlternate;
	string causalSNPrsID;
	
	LDBlockPointerVector::const_iterator currentLD = local_LDBlockPointerVector->begin();
	const snpStatisticHash* statisticHashPtr = NULL;
	int panelNumber = 0;
	for (; currentLD != local_LDBlockPointerVector->end(); currentLD++, panelNumber++) // for each LDBlock
	{
		
		if ( !(*currentLD)->getTagNumber() && !(*currentLD)->getCandidateNumber() )
		{
			continue;
		}		
		
		/*************Access the Statistics Hash Table of the LDBlock*******************/
		statisticHashPtr = getStatisticHash(*currentLD);
		if (!statisticHashPtr)
		{
			continue; // skip this LDBlock
		}
		/****************************************************************************/
		
		/**************Check if LDBlock is Alternate or Null Panel*******************/
		isAlternate = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->size();
		if (isAlternate)
		{
			causalSNPrsID = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->at(0);
		}
		/****************************************************************************/
		
		/**************Get the tag and candidate SNPs of the LDBlock*****************/
		tags.clear();
		candidates.clear();
		getTagsFromLDBlock( *currentLD, tags );
		getCandidatesFromLDBlock(*currentLD, candidates);
		/****************************************************************************/
		
		/*************Get tagSNPs' statistics****************************************/
		sHat_tags.clear();
		getStatistics( statisticHashPtr, tags, sHat_tags );
		/****************************************************************************/		
		
		/*************Get candidateSNPs' statistics****************************************/
		candidateMAFs.clear();
		sHat_candidates.clear();
		getStatistics( statisticHashPtr, candidates, sHat_candidates );	
		local_Population->getMAF(candidates,candidateMAFs);
		/****************************************************************************/
		
		/*************Get the best-tag and its statistic*******************************/		
		followUpSNPs.clear();
		sHat.clear();
		vector<double>::iterator cor_it;
		int maxPos;
		for (int pos = 0; pos < candidates.size(); pos++)
		{
			local_Population->getCorrelation( candidates.at(pos), tags, correlations);
			cor_it = max_element( correlations.begin(), correlations.end(), absComp() );
			
			R.push_back( *cor_it );
			maxPos = int( cor_it - correlations.begin() );
			sHat.push_back( sHat_tags.at(maxPos) );
			
			followUpSNPs.push_back( (followUpSNP){0, 
				sHat_candidates.at(pos),
				causalSNPrsID == candidates.at(pos)->getrsID(),
				isAlternate,
				panelNumber,
				-1.0,
				-1.0,
				-1.0,
				-1.0,
				-1.0,
				candidates.at(pos)->getrsID()
			} );
			
		}
		/****************************************************************************/
		
		/*************Compute likelihood and update followupSNPs structs*************/
		
		vector<double> likelihood( followUpSNPs.size() );
		vector<double> causalLikelihoods( followUpSNPs.size() );
		vector<double> noncausalLikelihoods( followUpSNPs.size() );
		vector<double> variance( followUpSNPs.size() );
		vector<double> causalMean( followUpSNPs.size() );
		vector<double> noncausalMean( followUpSNPs.size() );		
		computeLikelihood( candidateMAFs, R, sHat, likelihood, causalLikelihoods, noncausalLikelihoods, variance, causalMean, noncausalMean);
		
		int i=0;
		for ( vector< followUpSNP >::iterator it = followUpSNPs.begin(); it != followUpSNPs.end(); it++, i++)
		{
			it->likelihood = likelihood.at( i );
			it->causalLikelihood = causalLikelihoods.at(i);
			it->noncausalLikelihood = noncausalLikelihoods.at(i);
			it->variance = variance.at(i);
			it->causalMean = causalMean.at(i);
			it->noncausalMean = noncausalMean.at(i);
			local_followUpSNPs.push_back( *it );
		}
		/****************************************************************************/
	}
	/***************Sort Follow-up SNPs******************************************/
	sort( local_followUpSNPs.begin(), local_followUpSNPs.end(), sortSNPs() );
	/****************************************************************************/
	time(&timeEnd);
	timeDiff = difftime( timeEnd, timeStart);
	cout << "RFSS Best Tag Selection: " << timeDiff << " seconds." << endl;
}


void RFSS::computeLikelihood(vector<double>& candidateMAFs,
														 vector<double>& R, 
														 vector<double>& sHat, 
														 vector<double>& likelihood,
														 vector<double>& causalLikelihood,
														 vector<double>& noncausalLikelihood,
														 vector<double>& variance,
														 vector<double>& causalMean,
														 vector<double>& noncausalMean
														 )
{
	double zero = 0.0;
	double one = 1.0;
	int N = likelihood.size();
	vector<double> causalMeanVec( N, zero);
	vector<double> noncausalMeanVec( N, zero);
	vector<double> onesVec( N, one);
	
	vector<double> NCPVec( N, causalSNPNCP);
	vector<double> RSquare( N, zero);
	vector<double> Variance(N, zero);
	
	vector<double> C_i(N, causalSNPProbability);
	vector<double> One_C_i(N, 1.0 - causalSNPProbability);
	
	vector<double> causalMixturePart(N, zero);
	vector<double> noncausalMixturePart(N, zero);
	
	/**causalSNP NCP is now MAF dependent****************************************/
	for (int i = 0; i < N; i++)
	{
		if ( candidateMAFs.at(i) > 0.5 )
		{
			NCPVec.at(i) *= -1.0; 
		}		
	}
	/****************************************************************************/
	
	/**Perfect LD lookup*********************************************************/
	vector<bool> isPerfectLD(N, false);
	vector<double> perfectLDLikelihood(N, 0.0);
	for (int i=0; i<N; i++)
	{
		if ( abs( R[i] ) > 0.98 )
		{
			isPerfectLD[i] = true;
			if ( abs( sHat[ i ] ) > significanceThreshold )
			{
				perfectLDLikelihood[i] = 1.1;
			}
			else 
			{
				perfectLDLikelihood[i] = 0.0;
			}
		}		
	}
	/****************************************************************************/
	
	
	
	
	
	/***************Means, Variance of mixture***********************************/
	vdMul(N, &R[0], &sHat[0], &noncausalMeanVec[0]); //Non causal mean  = r*shat
	copy(noncausalMeanVec.begin(), noncausalMeanVec.end(), noncausalMean.begin() );
	
	vdSqr(N, &R[0], &RSquare[0]); // RSquare vector
	vdSub(N, &onesVec[0], &RSquare[0], &Variance[0]); // 1 - r^2
	
	vdMul(N, &Variance[0], &NCPVec[0], &causalMeanVec[0]); // (1-r^2)*ncp
	vdAdd(N, &noncausalMeanVec[0], &causalMeanVec[0], &causalMeanVec[0]); // r*shat + (1-r^2)*ncp
	copy(causalMeanVec.begin(), causalMeanVec.end(), causalMean.begin() );
	
	
	vdSqrt(N, &Variance[0], &Variance[0]); // sqrt( 1 - r^2 )
	copy(Variance.begin(), Variance.end(), variance.begin() );
	/****************************************************************************/
	
	
	/*******************Causal Mixture Likelihood********************************/	
	vector<double> causal_Z_right( N, significanceThreshold);
	vector<double> causal_Z_left( N, -1.0* significanceThreshold);
	
	
	vdSub(N, &causal_Z_right[0], &causalMeanVec[0], &causal_Z_right[0]);
	vdDiv(N, &causal_Z_right[0], &Variance[0], &causal_Z_right[0]);
	
	vdSub(N, &causal_Z_left[0], &causalMeanVec[0], &causal_Z_left[0]);
	vdDiv(N, &causal_Z_left[0], &Variance[0], &causal_Z_left[0]);
	
	vdCdfNorm(N, &causal_Z_right[0], &causal_Z_right[0]);
	vdCdfNorm(N, &causal_Z_left[0], &causal_Z_left[0]);
	
	vdSub(N, &causal_Z_left[0], &causal_Z_right[0], &causalMixturePart[0]);
	vdAdd(N, &onesVec[0], &causalMixturePart[0], &causalMixturePart[0]);
	copy( causalMixturePart.begin(), causalMixturePart.end(), causalLikelihood.begin() );
	vdMul(N, &C_i[0],  &causalMixturePart[0],  &causalMixturePart[0]);
	/****************************************************************************/
	
	
	
	/********************Non Causal Mixture Likelihood**************************/
	
	vector<double> noncausal_Z_right( N, significanceThreshold);
	vector<double> noncausal_Z_left( N, -1.0* significanceThreshold);
	
	vdSub(N, &noncausal_Z_right[0], &noncausalMeanVec[0], &noncausal_Z_right[0]);
	vdDiv(N, &noncausal_Z_right[0], &Variance[0], &noncausal_Z_right[0]);
	
	vdSub(N, &noncausal_Z_left[0], &noncausalMeanVec[0], &noncausal_Z_left[0]);
	vdDiv(N, &noncausal_Z_left[0], &Variance[0], &noncausal_Z_left[0]);
	
	vdCdfNorm(N, &noncausal_Z_right[0], &noncausal_Z_right[0]);
	vdCdfNorm(N, &noncausal_Z_left[0], &noncausal_Z_left[0]);
	
	vdSub(N, &noncausal_Z_left[0], &noncausal_Z_right[0], &noncausalMixturePart[0]);
	vdAdd(N, &onesVec[0], &noncausalMixturePart[0], &noncausalMixturePart[0]);
	copy( noncausalMixturePart.begin(), noncausalMixturePart.end(), noncausalLikelihood.begin() );
	vdMul(N, &One_C_i[0],  &noncausalMixturePart[0],  &noncausalMixturePart[0]);
	/****************************************************************************/
	
	/**************Write Likelihood**********************************************/
	vdAdd(N, &causalMixturePart[0], &noncausalMixturePart[0], &likelihood[0]);
	
	for (int i = 0; i < N; i++)
	{
		if (isPerfectLD[i])
		{
			likelihood[i] = perfectLDLikelihood[i];
		}
	}	
	/****************************************************************************/
}


void RFSS::allTagSelection()
{
	/**********************************************************
	 * Use all tags and every correlated candidate
	 * to test a specific candidate SNP.
	 * NCP of tested SNP changes as we sweep each candidate
	 * NCP of tag SNPs change as we sweep correlated candidate
	 ***********************************************************/
	
	/**********************************************************
	 * 1) Get the tags of a candidate
	 * 2) Remove the tags that are in perfect LD to candidate
	 * 3) Remove the tags that are in perfect LD to tags.
	 ***********************************************************/
	
	time_t timeStart, timeEnd;
	double timeDiff;
	time(&timeStart);
	
	
	local_followUpSNPs.clear();
	
	/****These store the tags and candidates in each LDBlock****/
	vector<const SNP*> tags;
	vector<const SNP*> candidates;
	/***********************************************************/
	
	/***********Various containers******************************/
	vector<followUpSNP> followUpSNPs;
	bool isAlternate;
	string causalSNPrsID;
	
	vector<double> Sigma_candidateSNPs; // Correlation matrix of the candidates.
	vector<double> Sigma_tagSNPs; // Correlation matrix of the tags.
	vector<double> sHat_tags; // tag SNPs' statistics
	vector<double> sHat_candidates; // candidate SNPs' statistics
	vector<double> R_candidateSNPs; // to tags each row is the candidate to tag
	vector<double> candidateMAFs; // Store the MAFs of candidates
	/***********************************************************/
	
	/***Access to LDBlocks and their statistic content**********/
	//LDBlockPointerVector::const_iterator currentLD = local_LDBlockPointerVector->begin();
	const snpStatisticHash* statisticHashPtr = NULL;
	/***********************************************************/
	int panelNumber=0;
	for (LDBlockPointerVector::const_iterator currentLD = local_LDBlockPointerVector->begin(); currentLD != local_LDBlockPointerVector->end(); currentLD++)
	{
		panelNumber++;
		if ( !(*currentLD)->getTagNumber() && !(*currentLD)->getCandidateNumber() )
		{
			continue;
		}
		
		/*************Access the Statistics Hash Table of the LDBlock*******************/
		statisticHashPtr = getStatisticHash(*currentLD);
		if (!statisticHashPtr)
		{
			continue; // skip this LDBlock
		}
		/****************************************************************************/
		
		/**************Check if LDBlock is Alternate or Null Panel*******************/
		isAlternate = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->size();
		if (isAlternate)
		{
			causalSNPrsID = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->at(0);
		}
		/****************************************************************************/
		
		/**************Get the SNP pointers of the LDBlock**********************/
		tags.clear();
		candidates.clear();
		getTagsFromLDBlock( *currentLD, tags );
		getCandidatesFromLDBlock(*currentLD, candidates);
		/****************************************************************************/
				
		/*************Get tagSNPs' statistics****************************************/
		//sHat_tags.clear();
		//getStatistics( statisticHashPtr, tags, sHat_tags );
		/****************************************************************************/		
		
		/*************Get candidateSNPs' statistics****************************************/
		//candidateMAFs.clear();
		sHat_candidates.clear();
		getStatistics( statisticHashPtr, candidates, sHat_candidates );
		//local_Population->getMAF(candidates, candidateMAFs);
		/****************************************************************************/
		
		
		/***************Fill up Follow-up SNPs***************************************/
		followUpSNPs.clear();
		for (int pos=0; pos < candidates.size(); pos++)
		{			
			followUpSNPs.push_back( 
														 (followUpSNP)
														 {
															 -1.0, 
															 sHat_candidates.at(pos), 
															 causalSNPrsID == candidates.at(pos)->getrsID(),
															 isAlternate,
															 panelNumber,
															 -1.0,
															 -1.0,
															 -1.0,
															 -1.0,
															 -1.0,
															 candidates.at(pos)->getrsID()
														 } 
														 );
		}
		/****************************************************************************/
		
		
		/*****Iterate over each candidate********************************************/
		// Get the top n tags for the candidate
		// Get the top m neighbor candidates for the candidate
		// Compute the likelihood
		vector<const SNP*> bestTags;
		vector<const SNP*> bestCandidates;
		for (int pos=0; pos < candidates.size(); pos++)
		{
			bestTags.clear();
			bestCandidates.clear();
			
			getBestTagSNPs(candidates.at(pos), tags, bestTags, dimTags);			
			if ( bestTags.size() == 0)
			{
				continue;
			}
			/**************Filter perfect LD tags****************************************/
			filterTagSNPs(bestTags);
			/****************************************************************************/			
			getBestCandidateSNPs(candidates.at(pos), candidates, bestCandidates, dimCandidates);
			
			/*************Get tagSNPs' statistics****************************************/
			sHat_tags.clear();
			getStatistics( statisticHashPtr, bestTags, sHat_tags );
			/****************************************************************************/		
			
			/*************Get candidateSNPs' MAF****************************************/
			candidateMAFs.clear();
			local_Population->getMAF(bestCandidates, candidateMAFs);
			/****************************************************************************/
						
			Sigma_tagSNPs.clear();
			Sigma_candidateSNPs.clear();
			R_candidateSNPs.clear();
			local_Population->getCorrelation(bestTags, bestTags, Sigma_tagSNPs);
			local_Population->getCorrelation(candidates.at(pos), bestCandidates, Sigma_candidateSNPs);
			local_Population->getCorrelation(bestTags, bestCandidates, R_candidateSNPs); // tags at rows, candidates at the columns
						
			computeLikelihoodMVN_single(candidateMAFs,
																	Sigma_candidateSNPs, 
																	Sigma_tagSNPs, 
																	sHat_tags,
																	R_candidateSNPs,
																	followUpSNPs.at(pos).likelihood,
																	followUpSNPs.at(pos).causalLikelihood,
																	followUpSNPs.at(pos).noncausalLikelihood,
																	followUpSNPs.at(pos).variance,
																	followUpSNPs.at(pos).causalMean,
																	followUpSNPs.at(pos).noncausalMean
																	);
			
			
			local_followUpSNPs.push_back( followUpSNPs.at(pos) );
		}
		
		
		/**************Filter perfect LD tags****************************************/
		//cout << "Filter perfect LD tags" << endl;
		//filterTagSNPs(tags);
		/****************************************************************************/
		
		
		/*************Get Correlation Matrices***************************************/
//		cout << "Get Correlation Matrices" << endl;
//		Sigma_tagSNPs.clear();
//		Sigma_candidateSNPs.clear();
//		R_candidateSNPs.clear();
//		local_Population->getCorrelation(tags, tags, Sigma_tagSNPs);
//		local_Population->getCorrelation(candidates, candidates, Sigma_candidateSNPs);
//		local_Population->getCorrelation(tags, candidates, R_candidateSNPs); // tags at rows, candidates at the columns
		/****************************************************************************/
		
		
		/*******Compute likelihoods**************************************************/
//		vector<double> likelihoods( followUpSNPs.size() );
//		vector<double> causalLikelihoods( followUpSNPs.size() );
//		vector<double> noncausalLikelihoods( followUpSNPs.size() );
//		vector<double> variance( followUpSNPs.size() );
//		vector<double> causalMean( followUpSNPs.size() );
//		vector<double> noncausalMean( followUpSNPs.size() );
//		cout << "Compute likelihoods" << endl;
		//computeLikelihoodMVN(candidateMAFs, Sigma_candidateSNPs, Sigma_tagSNPs, sHat_tags, R_candidateSNPs, likelihoods, causalLikelihoods, noncausalLikelihoods, variance, causalMean, noncausalMean);
		//computeLikelihoodMVN_fast( Sigma_candidateSNPs, Sigma_tagSNPs, sHat_tags, R_candidateSNPs, likelihoods, causalLikelihoods, noncausalLikelihoods, variance);
		/****************************************************************************/
		
		
		/******Get results and store to global follow-up SNPs*************************/
//		for (int pos=0; pos < candidates.size(); pos++)
//		{
//			followUpSNPs.at(pos).likelihood = likelihoods.at(pos);
//			followUpSNPs.at(pos).causalLikelihood = causalLikelihoods.at(pos);
//			followUpSNPs.at(pos).noncausalLikelihood = noncausalLikelihoods.at(pos);
//			followUpSNPs.at(pos).variance = variance.at(pos);
//			followUpSNPs.at(pos).causalMean = causalMean.at(pos);
//			followUpSNPs.at(pos).noncausalMean = noncausalMean.at(pos);
//			local_followUpSNPs.push_back( followUpSNPs.at(pos) );
//		}
		/****************************************************************************/
		
	}
	/***Sort the follow-up SNPs*********************************/
	sort( local_followUpSNPs.begin(), local_followUpSNPs.end(), sortSNPs() );
	/***********************************************************/
	
	time(&timeEnd);
	timeDiff = difftime( timeEnd, timeStart);
	cout << "RFSS All Tag Selection: " << timeDiff << " seconds." << endl;
	
}


void RFSS::computeLikelihoodMVN_single(vector<double>& candidateMAFs,
																			 vector<double>& Sigma_candidateSNPs, 
																			 vector<double>& Sigma_tagSNPs, 
																			 vector<double>& sHat_tags,
																			 vector<double>& R_candidateSNPs,
																			 double& likelihood,
																			 double& causalLikelihood,
																			 double& noncausalLikelihood,
																			 double& variance,
																			 double& causalMean,
																			 double& noncausalMean
																			 )
{
	/************************
	 * First candidate is
	 *
	 * the test SNP
	 ************************/
	double scale;
	
	int incx = 1;
	int incy = 1;
	int m,n,lda;
	int nrhs;
	int info;
	
	int N = candidateMAFs.size();
	int T = sHat_tags.size();
	
	int T2 = T*T;
	int NT = N*T;
	
	double* candidateNCPArray = new double[N];	
	double* onesArray = new double[N];
	
	double* Sigma_candidateSNPsArray = new double[N];
	double* Sigma_tagSNPsArray = new double[T2];
	double* LU_Sigma_tagSNPsArray = new double[T2];
	double* sHat_tagsArray = new double[T];
	double* R_candidateSNPsArray = new double[NT];
	
	double* associatedMeanArray = new double[N];	
	double nullMean;
	
	double* RSigmaInvArray = new double[T]; // Here only stores the test candidate
	double* RtagsNCPArray = new double[NT];
	
	double computedVariance;
	
	double null_Z_left;
	double null_Z_right;
	
	double* associated_Z_left = new double[N];
	double* associated_Z_right = new double[N];
	
	
	for (int i=0; i<N; i++)
	{
		onesArray[i] = 1.0;
	}
	
	/**Perfect skip*********************************************************/
//	for (int j=0; j<T; j++)
//	{
//		if ( R_candidateSNPs[j] > 0.95 )
//		{
//			R_candidateSNPs[j] = 0.95;
//		}
//		else if ( R_candidateSNPs[j] < -0.95 )
//		{
//			R_candidateSNPs[j] = -0.95;
//		}
//	}
	/****************************************************************************/
	
	/**Copy Vectors to Arrays****************************************************/
	copy( Sigma_candidateSNPs.begin(), Sigma_candidateSNPs.end(), Sigma_candidateSNPsArray );
	copy( Sigma_tagSNPs.begin(), Sigma_tagSNPs.end(), Sigma_tagSNPsArray );
	copy( sHat_tags.begin(), sHat_tags.end(), sHat_tagsArray );
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), R_candidateSNPsArray );
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), RtagsNCPArray );
	n = T;
	incx = 1;
	incy = 1;
	dcopy(&n, R_candidateSNPsArray, &incx, RSigmaInvArray, &incy); // copy the correlation of the test candidate to the tags.
	/****************************************************************************/
	
	/***Diagonal Trick************************************************************/
	incx = 1;
	incy = 1;
	n = T2;
	//eigenCleaning(Sigma_tagSNPsArray, T);
	for (int i=0; i<T; i++)
	{
		Sigma_tagSNPsArray[i*T+i] = 1.0 + 1e-3;
	}
	dcopy( &n, Sigma_tagSNPsArray, &incx, LU_Sigma_tagSNPsArray, &incy);
	/****************************************************************************/
	
	
	/****Prepare the Correlation term for the test candidate********************/
	
	// Takes into account MAF correction
	incx = 1;
	n = T;
	scale = -1.0*causalSNPNCP;
	for (int i=0; i < N; i++)
	{
		candidateNCPArray[i] = causalSNPNCP;
		dscal(&n, &causalSNPNCP, &RtagsNCPArray[i*T], &incx);
	}
	
	
//	for (int i=0; i < N; i++)
//	{
//		if ( candidateMAFs.at(i) > 0.5 )
//		{
//			candidateNCPArray[i] = -1.0 * causalSNPNCP;
//			dscal(&n, &scale, &RtagsNCPArray[i*T], &incx);		
//		}
//		else 
//		{
//			candidateNCPArray[i] = causalSNPNCP;
//			dscal(&n, &causalSNPNCP, &RtagsNCPArray[i*T], &incx);
//		}
//	}
	
	m = T;
	n = T;
	lda = T;
	nrhs = 1;
	int* ipiv = new int[ T ];
	dgetrf( &m, &n, LU_Sigma_tagSNPsArray, &lda, ipiv, &info ); // LU decomposition
	if (info)
	{
		cerr << "RFSS:: computeLikelihoodMVN_single => Problem with factorization" << endl;
	}
		
	dgetrs( "N", &n, &nrhs, LU_Sigma_tagSNPsArray, &lda, ipiv, RSigmaInvArray, &lda, &info ); // Solve for the correlation term, this is infact Sigma^-1 %*% R_iT
	if (info)
	{
		cerr << "RFSS:: computeLikelihoodMVN_single => Problem with solver" << endl;
	}
	
	delete [] ipiv;
	/****************************************************************************/
	
	/***Prepare standart deviation terms*****************************************/
	incx = 1;
	incy = 1;
	n = T;
	computedVariance = ddot(&n, RSigmaInvArray, &incx, &R_candidateSNPsArray[0], &incy); // R'_iT %*% Sigma^-1 %*% R_iT
	
	computedVariance = 1.0 - computedVariance;
	if ( computedVariance < 0.0)
	{
		computedVariance = 0.1;
	}
	else 
	{
		computedVariance = sqrt(computedVariance);
	}
	variance = computedVariance;	
	/****************************************************************************/
	
	
	/***Prepare means ***********************************************************/
	incx = 1;
	incy = 1;
	n = T;
	nullMean = ddot(&n, sHat_tagsArray, &incx, RSigmaInvArray, &incy);
	
	/***********************************
	 * Each row is the causal candidate SNP
	 * Each column is the candidate SNP we're testing.
	 ***********************************/
	n = N;
	m = T;
	incx = 1;
	incy = 1;
	for (int j=0; j<N; j++) // assumed causal candidate SNP iteration
	{
		associatedMeanArray[j] =  nullMean + candidateNCPArray[j]*( Sigma_candidateSNPsArray[j] - ddot(&m, RSigmaInvArray, &incx, &R_candidateSNPsArray[j*T] ,&incy)); 
	}
	
	/****************************************************************************/
	
	
	/**Prepare Z-scores**********************************************************/
	incx = 1;
	null_Z_left = ( -significanceThreshold - nullMean ) / computedVariance;
	null_Z_right = ( significanceThreshold - nullMean ) / computedVariance;
	
	
	for (int j = 0; j < N; j++)
	{
		associated_Z_left[j] = (-significanceThreshold - associatedMeanArray[j] ) / computedVariance;
		associated_Z_right[j] = (significanceThreshold - associatedMeanArray[j] ) / computedVariance;
	}
	/****************************************************************************/
	
	/**Compute probabilities**store in left Z scores*****************************/
	vdCdfNorm(1, &null_Z_left, &null_Z_left);
	vdCdfNorm(1, &null_Z_right, &null_Z_right);
	null_Z_left += (1.0 - null_Z_right); 

	vdCdfNorm(N, associated_Z_left, associated_Z_left);
	vdCdfNorm(N, associated_Z_right, associated_Z_right);

	associated_Z_left[0] += (1.0 - associated_Z_right[0]);
	for (int j = 1; j < N; j++)
	{
		associated_Z_left[0] +=  associated_Z_left[j] + (1.0 - associated_Z_right[j]);
	} 
	/****************************************************************************/
	
	/***Compute Likelihoods******************************************************/
	double causalC = causalSNPProbability;
	double noncausalC = (1.0 - double(N)*causalC);
	
	
	/**For each candidate SNP compute likelihood*********************************/
	noncausalLikelihood = noncausalC*null_Z_left;
	causalLikelihood = causalC*associated_Z_left[0];
	likelihood = causalLikelihood + noncausalLikelihood;

	causalMean = associatedMeanArray[0];
	noncausalMean = nullMean;
	/****************************************************************************/
	
	
	/***Clean********************************************************************/
	delete [] candidateNCPArray;
	delete [] onesArray;
	
	delete [] Sigma_candidateSNPsArray;
	delete [] Sigma_tagSNPsArray;
	delete [] sHat_tagsArray;
	delete [] R_candidateSNPsArray;
	
	delete [] LU_Sigma_tagSNPsArray;
	
	delete [] RSigmaInvArray;
	delete [] RtagsNCPArray;
	delete [] associated_Z_left;
	delete [] associated_Z_right;
	delete [] associatedMeanArray;
	/****************************************************************************/
}




void RFSS::computeLikelihoodMVN(vector<double>& candidateMAFs,
																vector<double>& Sigma_candidateSNPs, 
																vector<double>& Sigma_tagSNPs, 
																vector<double>& sHat_tags,
																vector<double>& R_candidateSNPs,
																vector<double>& likelihoods,
																vector<double>& causalLikelihoods,
																vector<double>& noncausalLikelihoods,
																vector<double>& variance,
																vector<double>& causalMean,
																vector<double>& noncausalMean
																)
{
	double zero = 0.0;
	double scale;
	
	
	int incx = 1;
	int incy = 1;
	int m, n, lda;
	int nrhs;
	int info;
	
	
	int N = likelihoods.size();
	int T = sHat_tags.size();
	cout << N << " " << T << endl;
	
	int T2 = T*T;
	int N2 = N*N;
	int NT = N*T;
	
	double* candidateNCPArray = new double[N];
	
	double* onesArray = new double[N];
	double* likelihoodsArray = new double[N];
	
	double* Sigma_candidateSNPsArray = new double[N2];
	double* Sigma_tagSNPsArray = new double[T2];
	double* LU_Sigma_tagSNPsArray = new double[T2];
	double* sHat_tagsArray = new double[T];
	double* R_candidateSNPsArray = new double[NT];
	
	double* associatedMeanArray = new double[N*N];	
	double* nullMeanArray = new double[N];
	
	double* RSigmaInvArray = new double[NT];
	double* RtagsNCPArray = new double[NT];
	
	double* varianceArray = new double[N];
	
	double* null_Z_left = new double[N];
	double* null_Z_right = new double[N];
	
	double* associated_Z_left = new double[N2];
	double* associated_Z_right = new double[N2];
	
	
	for (int i=0; i<N; i++)
	{
		onesArray[i] = 1.0;
	}
	
	/**Perfect LD lookup*********************************************************/
	vector<bool> isPerfectLD(N, false);
	vector<double> perfectLDLikelihood(N, 0.0);
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<T; j++)
		{
			if ( abs( R_candidateSNPs[i*T + j] ) > 0.98 )
			{
				isPerfectLD[i] = true;
				if ( abs( sHat_tags[ j ] ) > significanceThreshold )
				{
					perfectLDLikelihood[i] = 1.1;
				}
				else 
				{
					perfectLDLikelihood[i] = 0.0;
				}

				break;
			}			
		}
	}
	/****************************************************************************/
	
	/**Copy Vectors to Arrays****************************************************/
	copy( Sigma_candidateSNPs.begin(), Sigma_candidateSNPs.end(), Sigma_candidateSNPsArray );
	copy( Sigma_tagSNPs.begin(), Sigma_tagSNPs.end(), Sigma_tagSNPsArray );
	copy( sHat_tags.begin(), sHat_tags.end(), sHat_tagsArray );
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), R_candidateSNPsArray );
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), RtagsNCPArray );
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), RSigmaInvArray );
	/****************************************************************************/
	
	/***Diagonal Trick************************************************************/
	incx = 1;
	incy = 1;
	n = T2;
	//eigenCleaning(Sigma_tagSNPsArray, T);
	for (int i=0; i<T; i++)
	{
		Sigma_tagSNPsArray[i*T+i] += 5e-2;
	}
	dcopy( &n, Sigma_tagSNPsArray, &incx, LU_Sigma_tagSNPsArray, &incy);
	/****************************************************************************/
	
	
	/****Prepare the Correlation term for each test candidate********************/
	
	// Takes into account MAF correction
	incx = 1;
	n = T;
	scale = -1.0*causalSNPNCP;
	
	for (int i=0; i < N; i++)
	{
		if ( candidateMAFs.at(i) > 0.5 )
		{
			candidateNCPArray[i] = -1.0 * causalSNPNCP;
			dscal(&n, &scale, &RtagsNCPArray[i*T], &incx);		
		}
		else 
		{
			candidateNCPArray[i] = causalSNPNCP;
			dscal(&n, &causalSNPNCP, &RtagsNCPArray[i*T], &incx);
		}
	}
	// // // 
	
	m = T;
	n = T;
	lda = T;
	nrhs = 1;
	int* ipiv = new int[ T ];
	dgetrf( &m, &n, LU_Sigma_tagSNPsArray, &lda, ipiv, &info ); // LU decomposition
	if (info)
	{
		cerr << "RFSS:: computeLikelihoodMVN => Problem with factorization" << endl;
	}
	for (int i=0; i < N; i++)
	{		
		dgetrs( "N", &n, &nrhs, LU_Sigma_tagSNPsArray, &lda, ipiv, &RSigmaInvArray[i*T], &lda, &info ); // Solve for the correlation term, this is infact Sigma^-1 %*% R_iT
		if (info)
		{
			cerr << "RFSS:: computeLikelihoodMVN => Problem with solver" << endl;
		}
	}
	delete [] ipiv;
	/****************************************************************************/
	
	/***Prepare standart deviation terms*****************************************/
	incx = 1;
	incy = 1;
	n = T;
	for (int i=0; i<N; i++)
	{
		if ( isPerfectLD[i] )
		{
			varianceArray[i] = 0.0;
			continue;
		}		
		varianceArray[i] = ddot(&n, &RSigmaInvArray[i*T], &incx, &R_candidateSNPsArray[i*T], &incy); // R'_iT %*% Sigma^-1 %*% R_iT
	}
	vdSub(N, onesArray, varianceArray, varianceArray);
	vdSqrt(N, varianceArray, varianceArray);
	copy(varianceArray, varianceArray+N, variance.begin());
	/****************************************************************************/
	
	
	/***Prepare means ***********************************************************/
	incx = 1;
	incy = 1;
	n = T;
	for (int i=0; i<N; i++)
	{
		if ( isPerfectLD[i] )
		{
			nullMeanArray[i]	= 0.0;
			continue;
		}
		nullMeanArray[i] = ddot(&n, sHat_tagsArray, &incx, &RSigmaInvArray[i*T], &incy);
	}
	/***********************************
	 * Each row is the causal candidate SNP
	 * Each column is the candidate SNP we're testing.
	 ***********************************/
	n = N;
	m = T;
	incx = 1;
	incy = 1;
	for (int i=0; i<N; i++) // test candidate
	{
		if ( isPerfectLD[i] )
		{
			dscal(&n, &zero, &associatedMeanArray[i*N], &incx);
			continue;
		}		
		
		for (int j=0; j<N; j++) // assumed causal candidate SNP iteration
		{
			associatedMeanArray[i*N + j] =  nullMeanArray[i] + candidateNCPArray[j]*( Sigma_candidateSNPsArray[i*N + j] - ddot(&m, &RSigmaInvArray[i*T], &incx, &R_candidateSNPsArray[j*T] ,&incy)); 
		}
	}
	/****************************************************************************/
	
	
	/**Prepare Z-scores**********************************************************/
	incx = 1;
	for (int i =0; i < N; i++)
	{
		if ( isPerfectLD[i] )
		{
			null_Z_left[i] = 0.0;	
			null_Z_right[i] = 0.0;
			continue;
		}		
		null_Z_left[i] = ( -significanceThreshold - nullMeanArray[i] ) / varianceArray[i];
		null_Z_right[i] = ( significanceThreshold - nullMeanArray[i] ) / varianceArray[i];
	}
	
	for (int i=0; i< N; i++)
	{
		if ( isPerfectLD[ i ] )
		{
			dscal(&N, &zero, &associated_Z_left[i*N], &incx);
			dscal(&N, &zero, &associated_Z_right[i*N], &incx);
			continue;
		}
		
		for (int j = 0; j < N; j++)
		{
			associated_Z_left[i*N + j] = (-significanceThreshold - associatedMeanArray[i*N + j] ) / varianceArray[i];
			associated_Z_right[i*N + j] = (significanceThreshold - associatedMeanArray[i*N + j] ) / varianceArray[i];
		}
	}
	/****************************************************************************/
	
	/**Compute probabilities**store in left Z scores*****************************/
	vdCdfNorm(N, null_Z_left, null_Z_left);
	vdCdfNorm(N, null_Z_right, null_Z_right);
	for (int i=0; i < N; i++)
	{
		if ( isPerfectLD[ i ] )
		{
			continue;
		}
		
		null_Z_left[i] += (1.0 - null_Z_right[i]); 
	}
	vdCdfNorm(N2, associated_Z_left, associated_Z_left);
	vdCdfNorm(N2, associated_Z_right, associated_Z_right);
	for (int i=0; i< N; i++)
	{
		if ( isPerfectLD[i] )
		{
			continue;
		}
		
		associated_Z_left[i*N] += (1.0 - associated_Z_right[ i*N ]);
		
		for (int j = 1; j < N; j++)
		{
			associated_Z_left[i*N] +=  (associated_Z_left[ i*N + j] + 1.0 - associated_Z_right[ i*N + j ] );
		} 
	}
	/****************************************************************************/
	
	/***Compute Likelihoods******************************************************/
	double causalC = causalSNPProbability;
	double noncausalC = (1.0 - double(N)*causalC);
	incx=1;
	dscal(&N, &zero, likelihoodsArray, &incx);
	
	
	/**For each candidate SNP compute likelihood*********************************/
	for (int i=0; i<N; i++)
	{
		if (isPerfectLD[i])
		{
			causalLikelihoods[i] = 1.0;
			noncausalLikelihoods[i] = 0.0;
			likelihoods[i] = perfectLDLikelihood[i];
		}
		else 
		{
			causalLikelihoods[i] = causalC*associated_Z_left[i*N];
			noncausalLikelihoods[i] = noncausalC*null_Z_left[i];
			likelihoods[i] = causalC*associated_Z_left[i*N] + noncausalC*null_Z_left[i];
		}
	}
	copy( nullMeanArray, nullMeanArray + N, noncausalMean.begin() );
	for (int i=0; i<N; i++)
	{
		causalMean[i] = associatedMeanArray[i*N + i];
	}
	/****************************************************************************/
	
	
	/***Clean********************************************************************/
	
	
	delete [] candidateNCPArray;
	delete [] onesArray;
	delete [] likelihoodsArray;
	
	delete [] Sigma_candidateSNPsArray;
	delete [] Sigma_tagSNPsArray;
	delete [] sHat_tagsArray;
	delete [] R_candidateSNPsArray;
	
	
	delete [] nullMeanArray;
	delete [] LU_Sigma_tagSNPsArray;
	delete [] varianceArray;
	delete [] null_Z_left;
	delete [] null_Z_right;
	
	delete [] RSigmaInvArray;
	delete [] RtagsNCPArray;
	delete [] associated_Z_left;
	delete [] associated_Z_right;
	delete [] associatedMeanArray;
	/****************************************************************************/
		
}




void RFSS::computeLikelihoodMVN_fast(vector<double>& candidateMAFs,
																		 vector<double>& Sigma_candidateSNPs, 
																		 vector<double>& Sigma_tagSNPs, 
																		 vector<double>& sHat_tags,
																		 vector<double>& R_candidateSNPs,
																		 vector<double>& likelihoods,
																		 vector<double>& causalLikelihoods,
																		 vector<double>& noncausalLikelihoods,
																		 vector<double>& variance,
																		 vector<double>& causalMean,
																		 vector<double>& noncausalMean
																		 )
{
	
	int N = likelihoods.size();
	int T = sHat_tags.size();
	
	int incx = 1;
	int incy = 1;
	int n;
	double scale, det;
	
	
	/***Dynamic Storage***********************************************************/
	int* ipiv = new int[ T+1 ];	
	double* Stag = new double[ T ];
	double* SigmaTag = new double[ T*T ];
	double* RCandidate2Tag = new double[ N*T ];
	double* RNCP_Stag = new double[ N*T ];
	double* SigmaCandidateNCP = new double[ N*N ];
	double* VarianceDeterminantMatrix = new double[ (T+1) * (T+1) ];
	double* VarianceDeterminantWork = new double[ (T+1) * (T+1) ];
	double* StandartDeviationArray = new double[ N ];
	double* MeanDeterminantMatrix = new double[ (T+1) * (T+1) ];
	double* MeanDeterminantWork = new double[  (T+1) * (T+1) ];
	double* MeanArray = new double[ N*N ];
	double* nullMeanArray = new double[ N ];
	double* SigmaTagDeterminantMatrix = new double[T*T];
	double SigmaTagDeterminantValue = 1.0;
	
	
	double* null_Z_left = new double[N];
	double* null_Z_right = new double[N];
	
	double* associated_Z_left = new double[N*N];
	double* associated_Z_right = new double[N*N];	
	/****************************************************************************/
	
	
	/**Perfect LD lookup*********************************************************/
	vector<bool> isPerfectLD(N, false);
	vector<double> perfectLDLikelihood(N, 0.0);
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<T; j++)
		{
			if ( abs( R_candidateSNPs[i*T + j] ) > 0.99 )
			{
				isPerfectLD[i] = true;
				perfectLDLikelihood[i] = double( abs( sHat_tags[ j ] ) > significanceThreshold );
				break;
			}			
		}
	}
	/****************************************************************************/
	
	
	
	/****************************************************************************/
	copy( Sigma_tagSNPs.begin(), Sigma_tagSNPs.end(), SigmaTag);
	copy( Sigma_candidateSNPs.begin(), Sigma_candidateSNPs.end(), SigmaCandidateNCP );
	copy( sHat_tags.begin(), sHat_tags.end(), Stag);
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), RCandidate2Tag);
	copy( R_candidateSNPs.begin(), R_candidateSNPs.end(), RNCP_Stag);
	
	
	incx = 1;
	n = N*T;
	scale = causalSNPNCP;
	dscal(&n, &scale, RNCP_Stag, &incx); // scale with lambda_c
	
	scale = -1.0;
	n = T;
	incx = 1;
	incy = 1;
	for (int i=0; i<N; i++)
	{
		daxpy(&n, &scale, Stag, &incx, &RNCP_Stag[i*T], &incy ); // R*Lambda_c - Stag
	}
	
	incx = 1;
	n = N*N;
	scale = causalSNPNCP;
	dscal(&n, &scale, SigmaCandidateNCP, &incx); // scale with lambda_c
	/****************************************************************************/
	
	
	/****Sigma Tag Determinant***************************************************/
	
	eigenCleaning( SigmaTag, T);
	incx = 1;
	incy = 1;
	n = T*T;
	dcopy(&n, SigmaTag, &incx, SigmaTagDeterminantMatrix, &incy);
	
	SigmaTagDeterminantValue = detCholesky(SigmaTagDeterminantMatrix, T);
	
	/****************************************************************************/
	
	/****Compute (T+1)x(T+1) Variance Determinants*******************************/
	n = T;
	incx = 1;
	incy = 1;
	for (int i=0; i<T; i++) // Copy Sigma Tag to VarianceDeterminantMatrix bottom right
	{
		dcopy(&n, &SigmaTag[i*T], &incx, &VarianceDeterminantMatrix[ (i+1)*(T+1) + 1], &incy);
	}
	VarianceDeterminantMatrix[0] = 1.0;
	
	for (int i=0; i<N; i++) // for each candidate compute the determinant. This matrix will also be symmetric
	{
		if (isPerfectLD[i] )
		{
			continue;
		}
		// first copy R_i to both locations
		
		n = T;
		incx = 1;
		incy = 1;
		dcopy(&n, &RCandidate2Tag[i*T], &incx, &VarianceDeterminantMatrix[1], &incy);
		
		incy = T+1;
		dcopy(&n, &RCandidate2Tag[i*T], &incx, &VarianceDeterminantMatrix[T+1], &incy);
		
		
		// move to Work Array
		n = (T+1)*(T+1);
		incx = 1;
		incy = 1;
		dcopy(&n, VarianceDeterminantMatrix, &incx, VarianceDeterminantWork, &incy);
		
		eigenCleaning(VarianceDeterminantWork, T+1);
		det = detLU(VarianceDeterminantWork, T+1);
		if ( det < 0 )
		{
				printMatrix(VarianceDeterminantMatrix, T+1, 0, T+1);
			break;
		}
		//Store the variance term for the ith candidate
		StandartDeviationArray[i] = sqrt( det / SigmaTagDeterminantValue );
		variance[i] = StandartDeviationArray[i];
	}
	/****************************************************************************/
	
	/****Compute Mean Terms******************************************************/
	n = (T+1)*(T+1);
	incx = 1;
	incy = 1;
	dcopy(&n, VarianceDeterminantMatrix, &incx, MeanDeterminantMatrix, &incy); // Bottom rigth block is SigmaTag
	
	for (int i=0; i<N; i++) // For each test candidate
	{
		if (isPerfectLD[i] )
		{
			continue;
		}
		
		//Right top block is constant
		n = T;
		incx = 1;
		incy = T+1;
		dcopy(&n, &RCandidate2Tag[i*T], &incx, &MeanDeterminantMatrix[T+1], &incy);
		
		/***Compute null mean******************************************/
		n = (T+1)*(T+1);
		incx = 1;
		incy = 1;
		dcopy(&n, MeanDeterminantMatrix, &incx, MeanDeterminantWork, &incy); // Move to Work
		MeanDeterminantWork[0] = 0.0;
		
		n = T;
		incx = 1;
		incy = 1;
		scale = -1.0;
		dcopy(&n, Stag, &incx, &MeanDeterminantWork[1], &incy); // S_a
		dscal(&n, &scale, &MeanDeterminantWork[1], &incx); // -S_a
		
		
		det = detLU(MeanDeterminantWork, T+1);
		
		nullMeanArray[ i ] = det / SigmaTagDeterminantValue; // each column stores the test candidate
		/*************************************************************/
		
		for (int j=0; j<N; j++) // For each neighbor candidate that is assumed to be causal
		{
			n = (T+1)*(T+1);
			incx = 1;
			incy = 1;
			dcopy(&n, MeanDeterminantMatrix, &incx, MeanDeterminantWork, &incy); // Move to Work
			
			// copy top left and bottom left block
			MeanDeterminantWork[0] = SigmaCandidateNCP[ i*N + j ];
			
			n = T;
			incx = 1;
			incy = 1;
			dcopy(&n, &RNCP_Stag[ j*T ], &incx, &MeanDeterminantWork[1], &incy);
			
			// MeanDeterminantWork is not symmetrix, we use LU decomposition
			det = detLU(MeanDeterminantWork, T+1);
			MeanArray[ i*N + j ] = det / SigmaTagDeterminantValue; // each column stores the test candidate
		}
	}
	/****************************************************************************/
	
	
	
	
	/**Prepare Z-scores**********************************************************/
	for (int i=0; i < N; i++)
	{

		if ( isPerfectLD[ i ] )
		{
			continue;
		}

		null_Z_left[i] = ( -significanceThreshold - nullMeanArray[i] ) / StandartDeviationArray[i];
		null_Z_right[i] = ( significanceThreshold - nullMeanArray[i] ) / StandartDeviationArray[i];
	}
	
	for (int i=0; i< N*N; i++)
	{
		if ( isPerfectLD[ i / N ] )
		{
			i += N;
			continue;
		}
		associated_Z_left[i] = (-significanceThreshold - MeanArray[i] ) / StandartDeviationArray[ (i/N)*N ];
		associated_Z_right[i] = (significanceThreshold - MeanArray[i] ) / StandartDeviationArray[ (i/N)*N ];
	}
	
	
	/****************************************************************************/
	
	/**Compute probabilities**store in left Z scores*****************************/
	vdCdfNorm(N, null_Z_left, null_Z_left);
	vdCdfNorm(N, null_Z_right, null_Z_right);
	for (int i=0; i < N; i++)
	{
		if ( isPerfectLD[ i ] )
		{
			null_Z_left[i] = 0.0;
			continue;
		}
		
		null_Z_left[i] += 1.0 - null_Z_right[i]; 
	}
	
	
	vdCdfNorm(N*N, associated_Z_left, associated_Z_left);
	vdCdfNorm(N*N, associated_Z_right, associated_Z_right);

	for (int i=0; i< N*N; i++)
	{
		if ( isPerfectLD[ (i/N)*N ] )
		{
			i += N;
			continue;
		}
		if ( (i/N)*N == i )
		{
			associated_Z_left[ (i/N)*N ] +=  1.0 - associated_Z_right[i];	
		}
		else 
		{
			associated_Z_left[ (i/N)*N ] +=  associated_Z_left[i] + 1.0 - associated_Z_right[i];
		}
	}
	/****************************************************************************/
	
	
	
	
	/**For each candidate SNP compute likelihood*********************************/
	for (int i=0; i<N; i++) // For each test candidate
	{
		if (isPerfectLD[i] )
		{
			likelihoods[i] = perfectLDLikelihood[i];
			causalLikelihoods[i] = 1.0;
			noncausalLikelihoods[i] = 0.0;
			continue;
		}
		causalLikelihoods[i] = associated_Z_left[i*N];
		noncausalLikelihoods[i] = null_Z_left[i];		
		likelihoods[i] = causalSNPProbability*associated_Z_left[i*N] + (1.0 - double(N)*causalSNPProbability)*null_Z_left[i];		
	}
	copy( nullMeanArray, nullMeanArray + N, noncausalMean.begin() );
	n = N;
	incx = N;
	incy = 1;
	dcopy( &n, MeanArray, &incx, &causalMean[0], &incy);
	
	/****************************************************************************/
	
	/****Clean*******************************************************************/
	delete [] ipiv;
	delete [] Stag;
	delete [] SigmaTag;
	delete [] RCandidate2Tag;
	delete [] RNCP_Stag;
	delete [] SigmaCandidateNCP;
	delete [] VarianceDeterminantMatrix;
	delete [] VarianceDeterminantWork;
	delete [] StandartDeviationArray;
	delete [] MeanDeterminantMatrix;
	delete [] MeanDeterminantWork;
	delete [] MeanArray;
	delete [] nullMeanArray;
	delete [] SigmaTagDeterminantMatrix;
	
	
	delete [] null_Z_left;
	delete [] null_Z_right;
	
	delete [] associated_Z_left;
	delete [] associated_Z_right;	
	
	/****************************************************************************/
	
	
}



void RFSS::eigenCleaning( double* matrix, int N)
{
	/***********************************************
	* Computes eigenvectors and eigenvalues
	* Removes negative eigenvalues and projects back.
	***********************************************/
	int n = N, n2 = N*N, incx = 1, il, iu, m, lda = N, ldz = N, info, lwork, liwork;
	double abstol, vl, vu;
	int iwkopt;
	int* iwork = NULL;
	double wkopt;
	double* work = NULL;
	/* Local arrays */
	int* isuppz = new int[N*N];
	double* w = new double[N];
	double* z = new double[N*N];
	double* W = new double[N*N];
	double* temp = new double[N*N];
	double zero = 0.0, one = 1.0;
	
	il = 1;
	iu = N;
	/* Query and allocate the optimal workspace */
	lwork = -1;
	liwork = -1;
	dsyevr( "V", "A", "U", &n, matrix, &lda, &vl, &vu, &il, &iu,
				 &abstol, &m, w, z, &ldz, isuppz, &wkopt, &lwork, &iwkopt, &liwork,
				 &info );
	lwork = (int)wkopt;
	work = new double[lwork];
	liwork = iwkopt;
	iwork = new int[ liwork];
	/* Solve eigenproblem */
	
	bool converged = false;
	while( !converged )
	{
		dsyevr( "V", "A", "U", &n, matrix, &lda, &vl, &vu, &il, &iu,
					 &abstol, &m, w, z, &ldz, isuppz, work, &lwork, iwork, &liwork,
					 &info );
		if (info)
		{
			cerr << "eigenCleaning: " << info << endl;
		}
		
		if ( w[0] > 0)
		{
			converged = true;
		}
		else 
		{
			dscal(&n2, &zero, W, &incx);
			for (int i=0; i < N; i++)
			{
				if ( w[i] > 0.0 )
				{
					W[i*N + i] = w[i];
				}
				else 
				{
					W[i*N + i] = 0.0;
				}
			}
			
			/* Project back */
			dgemm("N", "N", &n, &n, &n, &one, z, &n, W, &n, &zero, temp, &n);
			dgemm("N", "T", &n, &n, &n, &one, temp, &n, z, &n, &zero, matrix, &n);
			for (int i=0; i < N; i++)
			{
				matrix[i*N + i] = 1.0;
			}			
		}
	}
	
	
	delete [] iwork;
	delete [] work;
	delete [] isuppz;
	delete [] w;
	delete [] z;
	delete [] W;	
	delete [] temp;
}



double RFSS::detCholesky( double* matrix, int dim)
{
	int n = dim;
	int lda = dim;
	int info;
	double detVal = 1.0;
	
	dpotrf( "L", &n, matrix, &lda, &info ); // Cholesky decomposition of the Sigma Tag SNPs
	if (info)
	{
		cerr << "Error in detCholesky: " << info << endl;
	}

	for (int i = 0; i<n; i++)
	{
		detVal *= matrix[ i*dim + i];
	}
	detVal *= detVal;
	return detVal;
}




double RFSS::detLU( double* matrix, int dim)
{
	int m = dim;
	int n = dim;
	int lda = dim;
	int info;
	int* ipiv = new int[dim];
	double det = 1.0;
	
	dgetrf( &m, &n, matrix, &lda, ipiv, &info );
	if (info)
	{
		cout << "Error in detLU: " << info << endl;
	}
	
	for (int i=0; i<n; i++)
	{
		if (ipiv[i] != i)
		{
			det = -det * matrix[i*n + i];
		}
		else 
		{
			det = det * matrix[i*n + i];
		}
	}
	return det;
}




