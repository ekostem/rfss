/*
 *  LDBlock.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/22/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */


#include "../include/LDBlock.h"

LDBlock::~LDBlock()
{
	tagSNPs.clear();
	candidateSNPs.clear();
}

LDBlock::LDBlock()
{
	info.assign( "" );
	chromosome = "UNKNOWN";
	startPosition = 0;
	stopPosition = 0;
}

LDBlock::LDBlock(const string& in, const string& chr, unsigned long start, unsigned long stop)
{
	info.assign( in );
	chromosome = chr;
	startPosition = start;
	stopPosition = stop;
}

LDBlock::LDBlock(const LDBlock& source)
{
	info.assign( source.getInfo() );
	chromosome = source.getChromosome();
	startPosition = source.getStartPosition();
	stopPosition = source.getStopPosition();
	
	//If not empty erase content and copy data from the source
	if (!tagSNPs.empty())
	{
		tagSNPs.clear();
	}
	tagSNPs = source.tagSNPs;
	
	if (!candidateSNPs.empty())
	{
		candidateSNPs.clear();
	}
	candidateSNPs = source.candidateSNPs;
}


LDBlock& LDBlock::operator=(const LDBlock& source)
{
	info.assign( source.getInfo() );
	chromosome = source.getChromosome();
	startPosition = source.getStartPosition();
	stopPosition = source.getStopPosition();
	
	
	//If not empty erase content and copy data from the source
	if (!tagSNPs.empty())
	{
		tagSNPs.clear();
	}
	tagSNPs = source.tagSNPs;
	
	if (!candidateSNPs.empty())
	{
		candidateSNPs.clear();
	}
	candidateSNPs = source.candidateSNPs;
	
	return *this;	
}

bool LDBlock::insertSNP( const tagSNP& tag )
{
	return tagSNPs.insert( tag ).second;	
}

bool LDBlock::removeSNP( const tagSNP& tag)
{
	return tagSNPs.erase( tag );
}


bool LDBlock::insertSNP( const candidateSNP& candidate )
{
	return candidateSNPs.insert( candidate ).second;
}


bool LDBlock::removeSNP( const candidateSNP& candidate)
{
	return candidateSNPs.erase( candidate );
}







