//
//  rfss_main.cpp
//  GWASLib_Xcode
//
//  Created by Emrah Kostem on 3/27/11.
//  Copyright 2011 _EMRAH_KOSTEM_. All rights reserved.
//

//#include "../include/TraditionalSNPSelection.h"
#include "../include/RFSS.h"
#include "../include/GWAS.h"
#include "../include/ReadGWAS.h"
#include <iostream>
#include <unistd.h>

using namespace std;

void print_help(void)
{
	cerr << "Usage: rfss [-x prefixName] [-d path] [-t #tagSNPs] [-n #candidateSNPs] [-c causalProb] [-s sig.Level] [-p power] [-o outputFile]" << endl;
	cerr << "\tAll parameters are required:" << endl;
	cerr << "\t[-x prefixName]      : Following files are required by rfss=> prefixName{.corr, .maf, .map, .tag, .cand}" << endl;
	cerr << "\tprefixName.corr      : pairwise correlations file (3 columns: rsID1 rsID2 Correlation)" << endl;
	cerr << "\tprefixName.maf       : minor allele frequencies file (2 columns: rsID MAF)" << endl;
	cerr << "\tprefixName.map       : map file (3 columns: rsID Chromosome Position)" << endl;
	cerr << "\tprefixName.tags      : tagSNPs file (3 columns: rsID Statistic Chromosome)" << endl;
	cerr << "\tprefixName.cands     : candidateSNPs file (2 columns: rsID Chromosome)" << endl;
	cerr << "\t[-d path]            : path to the required files" << endl;
	cerr << "\t[-t #tagSNPs]        : the number of the best tagSNPs to use during computations (e.g., 10)" << endl;
	cerr << "\t[-n #candidateSNPs]  : the number of the best neighboring candidateSNPs (e.g., 5)" << endl;
	cerr << "\t[-c causalProb]      : the probability of a candidateSNP being causal (e.g., 1e-6)" << endl;
	cerr << "\t[-s sig.Level]       : significance Level (e.g., 1e-8)" << endl;
	cerr << "\t[-p power]           : power at the causal SNP (e.g., 0.5)" << endl;
	cerr << "\t[-o outputFile]      : output filename" << endl;
}



int main(int argc, char* argv[])
{
	char *c_prefixName, *c_path, *c_outFilename, c;
	int numTags, numCandidates;
	double sigLevel, power, causalProb;
	
	while ( (c = getopt(argc, argv, "x:d:t:n:s:p:o")) != -1 )
	{
		switch (c) 
		{
			case 'x': // prefixName
				c_prefixName = optarg;
				break;
			case 'd': // path
				c_path = optarg;
				break;
			case 't': //numTags
				numTags = atoi(optarg);
				break;
			case 'n': //numCandidates
				numCandidates = atoi(optarg);
				break;
			case 'c': //causalProb
				causalProb = strtod( optarg , NULL );
				break;
			case 's': //significance Level
				sigLevel = strtod( optarg, NULL);
				break;
			case 'p': //power
				power = strtod( optarg, NULL);
				break;
			case 'o': //outputFile
				c_outFilename = optarg;
				break;
				
			default:
				cerr << "Error: Unknown option: " << c << endl;
				return -1;
		}
	}
	
	if (argc != optind + 8) 
	{
		print_help();
		return -1;
	}
	
	string path(c_path);
	string outputFile(c_outFilename);
	string prefixName(c_prefixName);
	
	
	
	
	
	WTCCC* T2D = NULL;
	GWAS* gwas = NULL;
	Population* CEU = NULL;
	
	RFSS* rfss = NULL;
	
	CEU = new Population("UKNOWN",
						 path + prefixName + ".corr",
						 path + prefixName + ".maf",
						 path + prefixName + ".map"
						 );		
	
	
	
	
	T2D = new WTCCC();
	T2D->registerPopulation(*CEU);
	T2D->setGwasFilename(path + prefixName + ".tag");
	T2D->setGwasCandidateSNPFilename(path + prefixName + ".cand");
	T2D->readData();
	
	gwas = new GWAS();
	gwas->readAllocation(*T2D);
	gwas->readData(*T2D);
	
	
	
	rfss = new RFSS();
	rfss->setCausalSNPProbability( causalProb );
	rfss->setSignificanceLevel( sigLevel );
	rfss->setCausalSNPPower( power );
	rfss->setTagCandidateDimension(numTags, numCandidates);
	rfss->usePopulation(*CEU);
	
	gwas->writeData(*rfss);
	
	rfss->allTagSelection();
	rfss->dumpResults( outputFile );
	
	
	
	delete rfss;
	
	delete CEU;
	delete gwas;
	delete T2D;
	
	return 0;
}
