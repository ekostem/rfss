/*
 *  main.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/21/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */


#include "../include/TraditionalSNPSelection.h"
#include "../include/RFSS.h"
#include "../include/GWAS.h"
#include "../include/SimulateGWAS.h"
#include <iostream>
#include <unistd.h>
using namespace std;



void print_help(void)
{
	cerr << "Usage: rfss [-x prefixName] [-d path] [-i simPop] [-g selPop] [-t #tagSNPs] [-n #candidateSNPs] [-c causalProb] [-s sig.Level] [-p power] [-o outputFile]" << endl;
	cerr << "\tAll parameters are required:" << endl;
	cerr << "\t[-x prefixName]      : Following files are required by rfss=> prefixName{.corr, .maf, .map, .tag, .cand}" << endl;
	cerr << "\tprefixName.corr      : pairwise correlations file (3 columns: rsID1 rsID2 Correlation)" << endl;
	cerr << "\tprefixName.maf       : minor allele frequencies file (2 columns: rsID MAF)" << endl;
	cerr << "\tprefixName.map       : map file (3 columns: rsID Chromosome Position)" << endl;
	cerr << "\tprefixName.tags      : tagSNPs file (3 columns: rsID Statistic Chromosome)" << endl;
	cerr << "\tprefixName.cands     : candidateSNPs file (2 columns: rsID Chromosome)" << endl;
	cerr << "\t[-d path]            : path to the required files" << endl;
	cerr << "\t[-t #tagSNPs]        : the number of the best tagSNPs to use during computations (e.g., 10)" << endl;
	cerr << "\t[-n #candidateSNPs]  : the number of the best neighboring candidateSNPs (e.g., 5)" << endl;
	cerr << "\t[-c causalProb]      : the probability of a candidateSNP being causal (e.g., 1e-6)" << endl;
	cerr << "\t[-s sig.Level]       : significance Level (e.g., 1e-8)" << endl;
	cerr << "\t[-p power]           : power at the causal SNP (e.g., 0.5)" << endl;
	cerr << "\t[-o outputFile]      : output filename" << endl;
}


int main(int argc, char* argv[])
{
	if ( argc < 11 )
	{
		cerr << "Usage: PATH SimulationPop AnalyzePop ENCODE_Region numAlternate numNull numPanel causalSNPProbability Power seed" << endl;
		cerr << "PATH                  : the path of the directory that contains the simulation data files." << endl;
		cerr << "Populations           : {CEU, CHB, YRI, JPT}" << endl;
		cerr << "ENCODE_Region         : {ENm010.7p15.2, ENm013.7q21.13, ENm014.7q31.33, ENr112.2p16.3, ENr113.4q26, ENr123.12q12, ENr131.2q37.1, ENr213.18q12.1, ENr232.9q34.11, ENr321.8q24.11}" << endl;
		cerr << "numAlternate          : the number of alternate regions (with a causal SNP) per panel." << endl;
		cerr << "numNull               : the number of null regions (with NO causal SNP) per panel." << endl;
		cerr << "numPanel              : the number of panels." << endl;
		cerr << "causalSNPProbability  : the probability of each SNP being causal." << endl;
		cerr << "Power                 : the power at the causal SNP." << endl;
		cerr << "seed                  : the seed for the random number generator." << endl;
		return -1;
	}
	
	
	string path( argv[1] );
	string simulationPopulation( argv[2] );
	string selectionPopulation( argv[3] );	
	string region( argv[4] );
	int numAlternate = atoi( argv[5] );
	int numNull = atoi( argv[6] );
	int numPanel = atoi( argv[7] );
	double causalProb = strtod( argv[8] , NULL );
	double power = strtod( argv[9] , NULL );
	int seed = atoi( argv[10] );
	
	string suffix = "_" + region + "_" + simulationPopulation + "_" + selectionPopulation + "_" + string(argv[5]) + "_" + string(argv[6]) + ".txt";
	
	GenlibSimulation* genTest = NULL;
	GWAS* gwasTest = NULL;
	Population* CEU = NULL;
	
	RFSS* rfss = NULL;
	CorrelationApproach* Corr = NULL;
	DistanceApproach* Dist = NULL;
	
	CEU = new Population(selectionPopulation,
											 path + "/pairwisecorrelations/" + selectionPopulation + "/ld_" + region + "_" + selectionPopulation + ".txt",
											 path + "/bglFiles/" + selectionPopulation + "/" + region + "_" + selectionPopulation + ".maf",
											 path + "/bglFiles/" + selectionPopulation + "/" + region + "_" + selectionPopulation + ".map"
											 );
	cout << "Read Population Data" << endl;
	cout << endl;
	
	
	genTest = new GenlibSimulation();
	genTest->setMafPath(path + "/bglFiles/" + simulationPopulation + "/");
	genTest->setMapPath(path + "/bglFiles/" + simulationPopulation + "/");
	genTest->setPopulation(simulationPopulation);
	genTest->setRegion(region);
	genTest->setGenlibPath(path + "/simulationFiles/");
	genTest->setAlternateNumber( numAlternate );
	genTest->setNullNumber( numNull );
	genTest->setPanelNumber( numPanel );
	genTest->setRandomGeneratorSeed(seed);
	genTest->generateData();

	cout << "Read Genlib Data and Generated Data" << endl;
	cout << endl;

	
	
	
	gwasTest = new GWAS();
	gwasTest->setStudyName("Test Study");
	gwasTest->setInfo("Hopefully will crash");
	gwasTest->readAllocation(*genTest);
	gwasTest->readData(*genTest);
	cout << "Read GWAS Data" << endl;
	cout << endl;

	
	
	
	/*******************************************/
//	Corr = new CorrelationApproach();
//	Corr->setSignificanceLevel(1e-8);
//	Corr->usePopulation(*CEU);
//	gwasTest->writeData(*Corr);
//	
//	Corr->setCorrelationThreshold(0.9);
//	Corr->correlationSelection();
//	Corr->dumpResults(path + "/Results/Corr_09" + suffix);
//	
//	Corr->setCorrelationThreshold(0.5);
//	Corr->correlationSelection();
//	Corr->dumpResults(path + "/Results/Corr_05" + suffix);
//
//	Corr->setCorrelationThreshold(0.1);
//	Corr->correlationSelection();
//	Corr->dumpResults(path + "/Results/Corr_01" + suffix);	
	/*******************************************/
	
	/*******************************************/
//	Dist = new DistanceApproach();
//	Dist->setSignificanceLevel(1e-8);
//	Dist->usePopulation(*CEU);
//	gwasTest->writeData(*Dist);
//	
//	Dist->setDistanceThreshold(1000);
//	Dist->distanceSelection();
//	Dist->dumpResults(path + "/Results/Dist_1k" + suffix);
//	
//	Dist->setDistanceThreshold(10000);
//	Dist->distanceSelection();
//	Dist->dumpResults(path + "/Results/Dist_10k" + suffix);
	/*******************************************/

	
	/*******************************************/
	rfss = new RFSS();
	rfss->setCausalSNPProbability(causalProb);
	rfss->setSignificanceLevel(1e-8);
	rfss->setCausalSNPPower(power);

	rfss->usePopulation(*CEU);
	gwasTest->writeData(*rfss);
	rfss->setTagCandidateDimension(1, 1);
	rfss->allTagSelection();
	rfss->dumpResults2(path + "/Results/Rfss_best_ncp" + suffix);
	
	rfss->setTagCandidateDimension(10, 10);
	rfss->allTagSelection();
	rfss->dumpResults2(path + "/Results/mRfss_ncp" + suffix);
	/*******************************************/
	
	/****Clean Dynamic Data*********************/
	delete Corr;
	delete Dist;	
	delete rfss;
	delete CEU;
	delete gwasTest;
	delete genTest;
	/*******************************************/
	
	return 0;
}
