/*
 *  Utility.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/26/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */


#include "../include/Utility.h"

RandomVariable::RandomVariable(const int seed, const int brng)
{
	brng_ = brng;
	seed_ = seed;
	vslNewStream( &stream_, brng_, seed_ );
}

void RandomVariable::generate_normal(const int number_variables, const int mean, const int std, double* output, const int generate_method)
{
	vdRngGaussian( generate_method, stream_, number_variables, output, mean, std );
}

void RandomVariable::mvgenerate_normal(const int number_variables, int dimension, const double* mean, double* variance_covariance_matrix, double* output, const int generate_method)
{
	int info;
	dpotrf("U", &dimension, variance_covariance_matrix, &dimension , &info);
	vdRngGaussianMV( generate_method, stream_, number_variables, output, dimension, VSL_MATRIX_STORAGE_FULL, mean, variance_covariance_matrix );
}

void RandomVariable::generate_uniform(const int number_variables, const double left, const double right, double* output, const int generate_method)
{
	vdRngUniform( generate_method, stream_, number_variables, output, left, right );
}


string itoa(int value, int base) 
{
	
	string buf;
	
	// check that the base if valid
	if (base < 2 || base > 16) return buf;
	
	enum { kMaxDigits = 35 };
	buf.reserve( kMaxDigits ); // Pre-allocate enough space.
	
	
	int quotient = value;
	
	// Translating number to string with base:
	do {
		buf += "0123456789abcdef"[ std::abs( quotient % base ) ];
		quotient /= base;
	} while ( quotient );
	
	// Append the negative sign
	if ( value < 0) buf += '-';
	
	reverse( buf.begin(), buf.end() );
	return buf;
}


