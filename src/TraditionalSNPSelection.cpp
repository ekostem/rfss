/*
 *  TraditionalSNPSelection.cpp
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 11/3/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#include "../include/TraditionalSNPSelection.h"


DistanceApproach::DistanceApproach() : AnalyzeGWAS()
{
	distanceThreshold = 1000;
	significanceLevel = 1e-8;
	significanceThreshold = 5.723;
}


DistanceApproach::~DistanceApproach()
{
	local_followUpSNPs.clear();
}

void DistanceApproach::setSignificanceLevel( double alpha )
{ 
	significanceLevel = alpha; 
	significanceThreshold = 1.0 - 0.5*significanceLevel;
	vdCdfNormInv(1, &significanceThreshold, &significanceThreshold);
}


void DistanceApproach::distanceSelection()
{
	time_t timeStart, timeEnd;
	double timeDiff;
	time(&timeStart);
	
	
	/***********************************************************************
	 * 1) For each LDBlock, sort tags by statistic strength
	 * 2) For each tag in the order, pair with the candidates using <distanceThreshold
	 * 3) Add to the local_followUpSNPs
	 ************************************************************************/

	local_followUpSNPs.clear();
	
	bool isAlternate;	
	string causalSNPrsID;
	
	
	vector<const SNP*> tags;
	vector<const SNP*> candidates;
	vector<double> sHat_tags;
	vector<double> sHat_candidates;
	vector<long> tagPositions;
	vector<long> candidatePositions;
	vector<tagSNPHelper> tagSNPsOrdered;
	
	LDBlockPointerVector::const_iterator currentLD = local_LDBlockPointerVector->begin();
	const snpStatisticHash* statisticHashPtr = NULL;
	
	int panelNumber = 0;
	for (; currentLD != local_LDBlockPointerVector->end(); currentLD++, panelNumber++) // for each LDBlock
	{
		
		if ( !(*currentLD)->getTagNumber() && !(*currentLD)->getCandidateNumber() ) // Check if valid LDBlock
		{
			continue;
		}		
		
		/*************Access the Statistics Hash Table of the LDBlock*******************/
		statisticHashPtr = getStatisticHash(*currentLD);
		if (!statisticHashPtr)
		{
			continue; // skip this LDBlock
		}
		/****************************************************************************/
		
		
		/**************Check if LDBlock is Alternate or Null Panel*******************/
		isAlternate = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->size();
		if (isAlternate)
		{
			causalSNPrsID = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->at(0);
		}
		/****************************************************************************/
		
		/**************Get the tag and candidate SNPs of the LDBlock*****************/
		tags.clear();
		candidates.clear();
		getTagsFromLDBlock( *currentLD, tags );
		getCandidatesFromLDBlock(*currentLD, candidates);
		/****************************************************************************/
		
		/*************Get tagSNPs' statistics****************************************/
		sHat_tags.clear();
		getStatistics( statisticHashPtr, tags, sHat_tags );
		/****************************************************************************/		
		
		/*************Get candidateSNPs' statistics**********************************/
		sHat_candidates.clear();
		getStatistics( statisticHashPtr, candidates, sHat_candidates );	
		/****************************************************************************/
		
		/*************Get tag and candidate positions *******************************/
		tagPositions.clear();
		candidatePositions.clear();
		
		tagPositions.resize( tags.size() );
		candidatePositions.resize( candidates.size() );
		
		local_Population->getPosition(tags, tagPositions);
		local_Population->getPosition(candidates, candidatePositions);
		/****************************************************************************/
		
		
		
		/***Sort tag SNPs based on statistics****************************************/
		tagSNPsOrdered.clear();
		for (int i = 0; i < tags.size(); i++)
		{	
			tagSNPsOrdered.push_back( 
															 (tagSNPHelper)
															 { 
																 sHat_tags.at(i),
																 tagPositions.at(i),
																 tags.at(i)
															 } 
															 );
		}
		sort( tagSNPsOrdered.begin(), tagSNPsOrdered.end(), tagSNPHelperSort() );
		/****************************************************************************/
		
		/***For each tag pair with candidates****************************************/
		vector<bool> isCandidatePaired( candidates.size(), false );
		
		for (int i = 0; i < tagSNPsOrdered.size(); i++)
		{
			for (int j = 0; j < candidates.size(); j++)
			{
				if ( !isCandidatePaired.at(j) && abs( tagSNPsOrdered.at(i).position - candidatePositions.at(j) ) < distanceThreshold )
				{
					local_followUpSNPs.push_back( 
																			 (followUpSNP) 
																			 {
																				 tagSNPsOrdered.at(i).statistic,
																				 abs( tagSNPsOrdered.at(i).position - candidatePositions.at(j) ),
																				 sHat_candidates.at(j),
																				 causalSNPrsID == candidates.at(j)->getrsID(),
																				 isAlternate,
																				 panelNumber
																			 } );
					isCandidatePaired.at(j) = true;
				}
			}
		}
		/****************************************************************************/
	}
	
	/***************Sort Follow-up SNPs******************************************/
	sort( local_followUpSNPs.begin(), local_followUpSNPs.end(), sortSNPs() );
	/****************************************************************************/
	time(&timeEnd);
	timeDiff = difftime( timeEnd, timeStart);
	cout << "Distance Selection: " << timeDiff << " seconds." << endl;
}

void DistanceApproach::dumpResults( const string& filename)
{

	if ( !local_followUpSNPs.size() )
	{
		return;
	}
	
	ofstream dumpFile;
	/******************************************************************************************
	 * Do the averaging for ties. Here for distance
	 ******************************************************************************************/
	pair<vector<followUpSNP>::iterator,vector<followUpSNP>::iterator> bounds;
	vector<followUpSNP>::iterator it = local_followUpSNPs.begin();
	
	
	dumpFile.open( filename.c_str() );
	dumpFile << "k\tPPV\tTag Statistic\tTag Distance\tCandidate Statistic\tisCandidateCausal\tPanel Number\n";
	
	int k = 1;
	double sum = 0.0;
	double avg = 0.0;
	
	int len = 0;
	do
	{
		bounds = equal_range(it, local_followUpSNPs.end(), *it, sortSNPs() );
		len = int(bounds.second - bounds.first);
		avg = 0;
		for (vector<followUpSNP>::iterator pt = bounds.first; pt != bounds.second; pt++)
		{
			if ( abs(pt->candidateStatistic) > significanceThreshold )
			{
				avg++;
			}
		}
		avg = avg / double(len);
		for (vector<followUpSNP>::iterator pt = bounds.first; pt != bounds.second; pt++)
		{
			sum += avg;
			
			dumpFile	<< k << "\t" // 1
			<< sum / double(k) << "\t" // 2
			<< pt->bestTagStatistic << "\t" // 3
			<< pt->bestTagDistance << "\t" // 4
			<< pt->candidateStatistic << "\t" // 5
			<< pt->isCandidateCausal << "\t" // 6
			<< pt->panelNumber << "\n"; // 7
			k++;
		}
		it = bounds.second;		
	} while ( it != local_followUpSNPs.end() );
	
	dumpFile.close();	
}


void DistanceApproach::dumpResults2( const string& filename )
{
	if ( !local_followUpSNPs.size() )
	{
		cout << "There are no follow-up SNPs" << endl;
		return;
	}
	
	ofstream dumpFile;
	
	
	dumpFile.open( filename.c_str() );
	dumpFile << "k\tPPV\tTag Statistic\tTag Correlation\tCandidate Statistic\tisCandidateCausal\tPanel Number\n";
	
	int k = 1;
	double sum = 0.0;
	for (vector<followUpSNP>::iterator pt = local_followUpSNPs.begin(); pt != local_followUpSNPs.end(); pt++)
	{
		
		if ( abs(pt->candidateStatistic) > significanceThreshold )
		{
			sum += 1.0;
		}
		
		dumpFile	<< k << "\t" // 1
		<< sum / double(k) << "\t" // 2
		<< pt->bestTagStatistic << "\t" // 3
		<< pt->bestTagDistance << "\t" // 4
		<< pt->candidateStatistic << "\t" // 5
		<< pt->isCandidateCausal << "\t" // 6
		<< pt->panelNumber << "\n"; // 7
		k++;
	}
	
	dumpFile.close();	
}




CorrelationApproach::CorrelationApproach() : AnalyzeGWAS()
{
	correlationThreshold = 0.9;
	significanceLevel = 1e-8;
	significanceThreshold = 5.723;
}

CorrelationApproach::~CorrelationApproach()
{
	local_followUpSNPs.clear();
}

void CorrelationApproach::setSignificanceLevel( double alpha )
{ 
	significanceLevel = alpha; 
	significanceThreshold = 1.0 - 0.5*significanceLevel;
	vdCdfNormInv(1, &significanceThreshold, &significanceThreshold);
}


void CorrelationApproach::correlationSelection()
{
	time_t timeStart, timeEnd;
	double timeDiff;
	time(&timeStart);
	
	
	/***********************************************************************
	 * 1) For each LDBlock, sort tags by statistic strength
	 * 2) For each tag in the order, pair with the candidates using correlationThreshold
	 * 3) Add to the local_followUpSNPs
	 ************************************************************************/

	local_followUpSNPs.clear();
	
	bool isAlternate;
	
	string causalSNPrsID;
	
	vector<const SNP*> tags;
	vector<const SNP*> candidates;
	vector<double> sHat_tags;
	vector<double> sHat_candidates;
	vector<double> correlations;
	vector<tagSNPHelper> tagSNPsOrdered;
	
	LDBlockPointerVector::const_iterator currentLD = local_LDBlockPointerVector->begin();
	const snpStatisticHash* statisticHashPtr = NULL;
	
	int panelNumber = 0;
	for (; currentLD != local_LDBlockPointerVector->end(); currentLD++, panelNumber++) // for each LDBlock
	{
		
		if ( !(*currentLD)->getTagNumber() && !(*currentLD)->getCandidateNumber() ) // Check if valid LDBlock
		{
			cerr << "CorrelationApproach, Skipped empty LDBlock" << endl;
			continue;			
		}		
		
		/*************Access the Statistics Hash Table of the LDBlock*******************/
		statisticHashPtr = getStatisticHash(*currentLD);
		if (!statisticHashPtr)
		{
			cerr << "CorrelationApproach, Skipped LDBlock, no statistic Hash Pointer" << endl;
			continue; // skip this LDBlock
		}
		/****************************************************************************/
		
		
		/**************Check if LDBlock is Alternate or Null Panel*******************/
		isAlternate = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->size();
		if (isAlternate)
		{
			causalSNPrsID = local_LDBlockCausalSNPHashTable->find( *currentLD )->second->at(0);
		}
		/****************************************************************************/

		/**************Get the tag and candidate SNPs of the LDBlock*****************/
		tags.clear();
		candidates.clear();
		getTagsFromLDBlock( *currentLD, tags );
		getCandidatesFromLDBlock(*currentLD, candidates);
		/****************************************************************************/
		
		/*************Get tagSNPs' statistics****************************************/
		sHat_tags.clear();
		getStatistics( statisticHashPtr, tags, sHat_tags );
		/****************************************************************************/		
		
		/*************Get candidateSNPs' statistics**********************************/
		sHat_candidates.clear();
		getStatistics( statisticHashPtr, candidates, sHat_candidates );	
		/****************************************************************************/
		
		/***Sort tag SNPs based on statistics****************************************/
		tagSNPsOrdered.clear();
		for (int i = 0; i < tags.size(); i++)
		{
			tagSNPsOrdered.push_back(
															 (tagSNPHelper)
															 { 
																 sHat_tags.at(i),
																 tags.at(i)
															 } 
															 );
		}
		sort( tagSNPsOrdered.begin(), tagSNPsOrdered.end(), tagSNPHelperSort() );
		/****************************************************************************/
		
		/***For pair each tag with candidates****************************************/
		vector<bool> isCandidatePaired( candidates.size(), false );
		
		for (int i = 0; i < tagSNPsOrdered.size(); i++)
		{
			correlations.clear();
			local_Population->getCorrelation( tagSNPsOrdered.at(i).tagPtr, candidates, correlations);
			for (int j = 0; j < candidates.size(); j++)
			{
					if ( !isCandidatePaired.at(j) && ( abs( correlations.at(j) ) > correlationThreshold ) )
					{
						local_followUpSNPs.push_back( 
																				 (followUpSNP) 
																				 {
																					 tagSNPsOrdered.at(i).statistic,
																					 correlations.at(j),
																					 sHat_candidates.at(j),
																					 causalSNPrsID == candidates.at(j)->getrsID(),
																					 isAlternate,
																					 panelNumber
																				 } 
																				 );
						
						isCandidatePaired.at(j) = true;
					}
			}
		}
		/****************************************************************************/
	}

	/***************Sort Follow-up SNPs******************************************/
	sort( local_followUpSNPs.begin(), local_followUpSNPs.end(), sortSNPs() );
	/****************************************************************************/
	time(&timeEnd);
	timeDiff = difftime( timeEnd, timeStart);
	cout << "Correlation Selection: " << timeDiff << " seconds." << endl;
}


void CorrelationApproach::dumpResults( const string& filename )
{
	if ( !local_followUpSNPs.size() )
	{
		cout << "There are no follow-up SNPs" << endl;
		return;
	}
	
	ofstream dumpFile;
	/******************************************************************************************
	 * Do the averaging for ties. Here for distance
	 ******************************************************************************************/
	pair<vector<followUpSNP>::iterator,vector<followUpSNP>::iterator> bounds;
	vector<followUpSNP>::iterator it = local_followUpSNPs.begin();
	
	
	dumpFile.open( filename.c_str() );
	dumpFile << "k\tPPV\tTag Statistic\tTag Correlation\tCandidate Statistic\tisCandidateCausal\tPanel Number\n";
	
	int k = 1;
	double sum = 0.0;
	double avg = 0.0;
	
	int len = 0;
	do
	{
		bounds = equal_range(it, local_followUpSNPs.end(), *it, sortSNPs() );
		len = int(bounds.second - bounds.first);
		avg = 0.0;
		for (vector<followUpSNP>::iterator pt = bounds.first; pt != bounds.second; pt++)
		{
			if ( abs(pt->candidateStatistic) > significanceThreshold )
			{
				avg++;
			}
		}
		avg = avg / double(len);
		for (vector<followUpSNP>::iterator pt = bounds.first; pt != bounds.second; pt++)
		{
			sum += avg;
			
			dumpFile	<< k << "\t"
			<< sum / double(k) << "\t"
			<< pt->bestTagStatistic << "\t"
			<< pt->bestTagCorrelation << "\t"
			<< pt->candidateStatistic << "\t"
			<< pt->isCandidateCausal << "\t"
			<< pt->panelNumber << "\n";
			k++;
		}
		it = bounds.second;		
	} while ( it != local_followUpSNPs.end() );
	
	dumpFile.close();	
}

void CorrelationApproach::dumpResults2( const string& filename )
{
	if ( !local_followUpSNPs.size() )
	{
		cout << "There are no follow-up SNPs" << endl;
		return;
	}
	
	ofstream dumpFile;
	
	
	dumpFile.open( filename.c_str() );
	dumpFile << "k\tPPV\tTag Statistic\tTag Correlation\tCandidate Statistic\tisCandidateCausal\tPanel Number\n";
	
	int k = 1;
	double sum = 0.0;
	for (vector<followUpSNP>::iterator pt = local_followUpSNPs.begin(); pt != local_followUpSNPs.end(); pt++)
	{
		
		if ( abs(pt->candidateStatistic) > significanceThreshold )
		{
			sum += 1.0;
		}
		
		dumpFile	<< k << "\t"
		<< sum / double(k) << "\t"
		<< pt->bestTagStatistic << "\t"
		<< pt->bestTagCorrelation << "\t"
		<< pt->candidateStatistic << "\t"
		<< pt->isCandidateCausal << "\t"
		<< pt->panelNumber << "\n";
		k++;
	}
	
	dumpFile.close();	
}



