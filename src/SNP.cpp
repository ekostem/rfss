/*
 *  SNP.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/21/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */


#include "../include/SNP.h"

SNP::SNP(const string& rsid)
{
	this->rsID.assign( rsid );
	this->statistic = 0.0;
}

SNP::SNP(const SNP& source)
{
	this->rsID.assign( source.rsID );
}

SNP& SNP::operator=(const SNP& source)
{
	this->rsID.assign( source.rsID );
	return *this;
}

bool SNP::operator==(const SNP& rhs) const
{
	return this->rsID == rhs.rsID;
	
}

bool SNP::operator<(const SNP& rhs) const
{
	return rsID < rhs.rsID;
}


long SNP::getPosition( Population& pop ) const
{
	long position = 0;
	pop.getPosition(this, position);
	return position;
}

double SNP::getMAF( Population& pop) const
{
	double maf = 0.0;
	pop.getMAF(this, maf);
	return maf;	
}


candidateSNP::candidateSNP(const string& rsid) : SNP(rsid)
{
	
}

candidateSNP::candidateSNP(const candidateSNP& source) : SNP(source)
{
	
}

candidateSNP& candidateSNP::operator=(const candidateSNP& source)
{
	SNP::operator=(source);
	return *this;
}


bool candidateSNP::operator==(const candidateSNP& rhs) const
{
	return SNP::operator==(rhs);
}


bool candidateSNP::operator<(const candidateSNP& rhs) const
{
	return SNP::operator<( rhs);
}




candidateSNP::~candidateSNP()
{
	
}


tagSNP::tagSNP(const string& rsid) : SNP(rsid)
{
	
}

tagSNP::tagSNP(const tagSNP& source) : SNP(source)
{
	
}

tagSNP& tagSNP::operator=(const tagSNP& source)
{
	SNP::operator=(source);
	return *this;
}

bool tagSNP::operator==(const tagSNP& rhs) const
{
	return SNP::operator==(rhs);
}

bool tagSNP::operator<(const tagSNP& rhs) const
{
	return SNP::operator<( rhs);
}

tagSNP::~tagSNP()
{
	
}

