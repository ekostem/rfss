#include "../include/GWAS.h"


GWAS::GWAS()
{
	numberofLDBlocks = 0;
	gwasName = "";
	gwasInfo = "";
}


GWAS::~GWAS()
{
	//delete the allocated space
	LDBlockPointerVector::iterator it;
	LDBlockStatisticsHash::iterator ld_it;
	LDBlockCausalProbabilityHash::iterator ld_cit;
	LDBlockCausalSNPHash::iterator ld_csit;
	
	for (it = LDBlocks.begin(); it != LDBlocks.end(); it++)
	{
		ld_it = LDBlockStatisticsHashTable.find( *it ); // get the block hashtable
		if ( ld_it != LDBlockStatisticsHashTable.end() )
		{
			if ( ld_it->second )
			{
				ld_it->second->clear(); //clear the hash table
				delete ld_it->second; // delete the hash table pointer				
			}
		}
		
		ld_cit = LDBlockCausalProbabilityHashTable.find( *it );
		if ( ld_cit != LDBlockCausalProbabilityHashTable.end() )
		{
			if ( ld_cit->second )
			{
				ld_cit->second->clear();
				delete ld_cit->second;
			}
		}
		
		ld_csit = LDBlockCausalSNPHashTable.find( *it );
		if ( ld_csit != LDBlockCausalSNPHashTable.end() )
		{
			if ( ld_csit->second )
			{
				ld_csit->second->clear(); //clear vector
				delete ld_csit->second; //delete vector pointer
			}
		}		
		delete (*it); //delete the ldblock pointer
	}
	
	LDBlockCausalSNPHashTable.clear();
	LDBlockCausalProbabilityHashTable.clear();
	LDBlockStatisticsHashTable.clear();
	LDBlocks.clear();
}

void GWAS::readAllocation( SimulateGWAS& simulation)
{
	simulation.writeAllocation(numberofLDBlocks);
	allocateSpace();
	cout << "GWAS, Space allocated. #LDBlocks:" << LDBlocks.size() << " HashTable size: " << LDBlockStatisticsHashTable.size() << endl;
}

void GWAS::readData( SimulateGWAS& simulation)
{
	simulation.writeData(LDBlocks,
											 LDBlockStatisticsHashTable,
											 LDBlockCausalProbabilityHashTable,
											 LDBlockCausalSNPHashTable
											 );
}


void GWAS::readAllocation( ReadGWAS& gwasRead)
{
	gwasRead.writeAllocation(numberofLDBlocks);
	allocateSpace();
	cout << "GWAS, Space allocated. #LDBlocks:" << LDBlocks.size() << " HashTable size: " << LDBlockStatisticsHashTable.size() << endl;
}

void GWAS::readData( ReadGWAS& gwasRead)
{
	gwasRead.writeData(LDBlocks,
											 LDBlockStatisticsHashTable,
											 LDBlockCausalProbabilityHashTable,
											 LDBlockCausalSNPHashTable
											 );
}




void GWAS::writeData( AnalyzeGWAS& analysis) const
{
	analysis.readData(LDBlocks,
										LDBlockStatisticsHashTable,
										LDBlockCausalProbabilityHashTable,
										LDBlockCausalSNPHashTable
										);
}



void GWAS::allocateSpace()
{
	LDBlock* block = NULL;
	snpStatisticHash* statHash = NULL;
	snpCausalProbabilityHash* causalHash = NULL;
	causalSNPrsIDVector* cSPV = NULL;
	
	if ( numberofLDBlocks > 0)
	{
		for (int i=0; i < numberofLDBlocks; i++ )
		{
			block = new LDBlock(); // create new block
			LDBlocks.push_back( block ); // add block to vector
			statHash = new snpStatisticHash(); // create new hash table for stats
			causalHash = new snpCausalProbabilityHash(); // create new hash table for causal probabilities.
			
			LDBlockStatisticsHashTable.insert( make_pair( block, statHash) );	// add the hash table to the ldblock	
			LDBlockCausalProbabilityHashTable.insert( make_pair( block, causalHash) ); // add the causal hash to the LDblock
			
			cSPV = new causalSNPrsIDVector; // allocate new causal SNP vector
			LDBlockCausalSNPHashTable.insert( make_pair( block, cSPV ) ); // register vector with the LDBlock
		}
	}
}

