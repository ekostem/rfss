/*
 *  SimulateGWAS.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/25/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */


//Simulate GWAS fill the statistics to the SNPs

#include "../include/SimulateGWAS.h"


SimulateGWAS::SimulateGWAS()
{
	
}


GenlibSimulation::GenlibSimulation() : SimulateGWAS()
{
	numTotalPanels = 0;
	numAlternatePerPanel = 0;
	numNullPerPanel = 0;
	
	randomGeneratorSeed = 0;
	
	mafPathName = "";
	mapPathName = "";
	populationName = "";
	regionName = "";
	genlibPathName = "";
	
	TAGSNPLabel = "tag";
	CANDIDATESNPLabel = "target";
	NULLSNPLabel = "dismiss";
	
	
}

GenlibSimulation::~GenlibSimulation()
{
	snpDataHashTable.clear();
}


void GenlibSimulation::writeAllocation( int& value) const
{
	value = (numAlternatePerPanel + numNullPerPanel)*numTotalPanels;
}

void GenlibSimulation::generateData()
{
	getHashTableData();
}


void GenlibSimulation::getHashTableData()
{
	/*
	 1) Determine tagSNPs, candidateSNPs and nullSNPs
	 2) Store their indices
	 3) Create LDBLock, fill with tag and candidate SNPs
	 4) Create (numAlternate + numNull)*totalNum copies
	 5) Randomly select causal SNPs for the alternate and null Panels.
	 6) Read the statistics from the files and assign to the SNPs in each LDBlock
	 */
	
	InputFile* mafFile = NULL;
	InputFile* mapFile = NULL;
	
//	time_t timeStart, timeEnd;
//	double timeDiff;
	
	candidateSNPIndices.clear();
	nullSNPIndices.clear();
	tagSNPIndices.clear();
	
//	time(&timeStart);
	try 
	{
		mafFile = new InputFile( mafPathName + regionName + "_" + populationName + ".maf" );
		cout << "Reading MAF file." << endl;
		for ( long line = 0; line < mafFile->length(); line++ )
		{
			int index( atoi( mafFile->at(line)->at(0).c_str() ) );
			double maf( strtod( mafFile->at(line)->at(2).c_str(), NULL) );
			string snpType( mafFile->at(line)->at(3) );
			
			snpDataTuple t(index, //index
										 maf, //MAF
										 snpType, // SNP type
										 -1 // position
										 );
			string rsid( mafFile->at(line)->at(1) );
			snpDataHashTable.insert( make_pair( rsid, t) );
			
			if ( snpType == CANDIDATESNPLabel )
			{
				candidateSNPIndices.push_back( index );
			}
			else if (snpType == NULLSNPLabel )
			{
				nullSNPIndices.push_back( index );
			}
			else if ( snpType == TAGSNPLabel )
			{
				tagSNPIndices.push_back( index );
			}
			else 
			{
				cerr << "Wrong type of SNP: " << snpType << endl;
			}
		}
		cout << "MAF file is read." << endl;
	}
	catch (InputFilexception& e) 
	{
		cerr << e.what() << endl;
	}
	if (mafFile)
	{
		delete mafFile;	
	}
//	time(&timeEnd);
//	timeDiff = difftime( timeEnd, timeStart);
//	cout << "Seconds: " << timeDiff << endl;
	
//	time(&timeStart);
	try 
	{
		mapFile = new InputFile( mapPathName + regionName + "_" + populationName + ".map" );
		cout << "Reading MAP file." << endl;
		snpDataHash::iterator it;
		snpDataHash::iterator it_end = snpDataHashTable.end();
		for ( long line = 0; line < mapFile->length(); line++ )
		{
			it = it_end;
			string rsid( mapFile->at(line)->at(0) );
			long position( atol( mapFile->at(line)->at(1).c_str() ) );
			it = snpDataHashTable.find( rsid );
			if ( it != it_end )
			{
				tr1::get<POSITION>( it->second ) = position;
			}
			else 
			{
				cout << rsid << " is not in map file" << endl;
			}
		}
		cout << "MAP file is read." << endl;
	}
	catch (InputFilexception& e) 
	{
		cerr << e.what() << endl;
	}
	if (mapFile)
	{
		delete mapFile;
	}
//	time(&timeEnd);
//	timeDiff = difftime( timeEnd, timeStart);
//	cout << "Seconds: " << timeDiff << endl;
	
	
	cout << "SNP data read. " << "Number of SNPs: " << snpDataHashTable.size() << endl;
}


void GenlibSimulation::writeData(LDBlockPointerVector& LDBlockVector,
																 LDBlockStatisticsHash& LDBlockStatisticsHashTable,
																 LDBlockCausalProbabilityHash& LDBlockCausalProbabilityHashTable,
																 LDBlockCausalSNPHash& LDBlockCausalSNPHashTable
																 )
{
	//Check for possible errors:
	if ( numTotalPanels == 0 )
	{
		cerr << "Total panel size is 0" << endl;
		return;
	}
	
	if ( !candidateSNPIndices.size() || !nullSNPIndices.size() )
	{
		cerr << "Number of candidate or null SNPs is 0" << endl;
		return;
	}
	
	if ( !LDBlockVector.size() )
	{
		cerr << "LDBlock container is empty" << endl;
		return;
	}
	
	if ( !LDBlockStatisticsHashTable.size() )
	{
		cerr << "LDBlock hashtable to statistics is empty" << endl;
	}
	
	
	time_t timeStart, timeEnd;
	double timeDiff;
	
	cout << "Generating simulation data" << endl;
	time(&timeStart);
	// Fill in the LDBlocks
	// Write the statistics for each SNP in each LDBlock
	
	// we assume total number of ldblocks are the same between the GWAS and the simulator
	int panelCount = (numAlternatePerPanel + numNullPerPanel)*numTotalPanels;
	int alternatePanelCount = numAlternatePerPanel*numTotalPanels;
	int nullPanelCount = numNullPerPanel*numTotalPanels;
	
	// These will store the causal SNP indices in each LDBlock
	vector<double> causalSNPPositions( alternatePanelCount );
	vector<double> nullSNPPositions( nullPanelCount );
		
	tr1::unordered_map<int, InputFile*> alternateGenlibFiles;
	tr1::unordered_map<int, InputFile*> nullGenlibFiles;
	
	RandomVariable* random_generator = NULL;
	
	if ( panelCount != LDBlockVector.size() )
	{
		cerr << "Panel number does not match to LDBlockVector size" << endl;
		return;
	}
	
	if ( panelCount != LDBlockStatisticsHashTable.size() )
	{
		cerr << "Panel number does not match to LDBlockStatisticsHashTable size" << endl;
		return;
	}
	
	// generate the alternate causal indices
	// generate the null causal indices
	
	random_generator = new RandomVariable(randomGeneratorSeed);
	random_generator->generate_uniform(alternatePanelCount, 0.0, double(candidateSNPIndices.size()), &causalSNPPositions[0]);
	vdFloor( alternatePanelCount , &causalSNPPositions[0], &causalSNPPositions[0] );
	delete random_generator;
	
	
	random_generator = new RandomVariable(randomGeneratorSeed);
	random_generator->generate_uniform(nullPanelCount, 0.0, double(nullSNPIndices.size()), &nullSNPPositions[0]);
	vdFloor( nullPanelCount , &nullSNPPositions[0], &nullSNPPositions[0] );
	delete random_generator;
	
	random_generator = new RandomVariable(randomGeneratorSeed);
	
	//hash the causal and null statistic files:
	InputFile* fp = NULL;
	for ( vector<int>::iterator it = candidateSNPIndices.begin(); it != candidateSNPIndices.end(); it++)
	{
		fp = new InputFile(genlibPathName + populationName + "/" + regionName + "_" + populationName + "-" + itoa(*it, 10) + ".sim.glt", 
											 NULL, //delimiter
											 "#" //skip character
											 );
		
		alternateGenlibFiles.insert( make_pair( *it, fp) );
	}
	
	for ( vector<int>::iterator it = nullSNPIndices.begin(); it != nullSNPIndices.end(); it++)
	{
		fp = new InputFile(genlibPathName + populationName + "/" + regionName + "_" + populationName + "-" + itoa(*it, 10) + ".sim.glt", 
											 NULL, //delimiter
											 "#" //skip character
											 );
		
		nullGenlibFiles.insert( make_pair( *it, fp) );
	}
	
	// Now fill each LDBlock with the tag and candidate SNPs (copy temp content)
	// For each LDBlock fill the statistics using the appropriate file
	
	LDBlock temp_LDBlock("Genlib", "UNKNOWN", 0,0);
	for (snpDataHash::iterator it = snpDataHashTable.begin(); it != snpDataHashTable.end(); it++)
	{
		string snpType( tr1::get<SNP_TYPE>( it->second ) );
		string rsid( it->first );
		
		if ( snpType == CANDIDATESNPLabel )
		{
			candidateSNP candidate(rsid);
			temp_LDBlock.insertSNP(candidate);
		}
		else if ( snpType == TAGSNPLabel )
		{
			tagSNP tag(rsid);
			temp_LDBlock.insertSNP(tag);
		}
	}
	
	for (LDBlockStatisticsHash::iterator it = LDBlockStatisticsHashTable.begin(); it != LDBlockStatisticsHashTable.end(); it++)
	{
		*(it->first) = temp_LDBlock; // copy temp LDBlock
	}
	
	int alternateCount = 0; //position of the alternate causal SNP in the candidateSNPIndices
	int nullCount = 0; // position of the null causal SNP int the nullSNPIndices
	int causalIndex; // index of the causal SNP
	double statisticReadLine; //line number to read from the causal SNP file
	
	tr1::unordered_map<int, InputFile*>::iterator alternateEnd = alternateGenlibFiles.end();
	tr1::unordered_map<int, InputFile*>::iterator nullEnd = nullGenlibFiles.end();
	tr1::unordered_map<int, InputFile*>::iterator fileHashPointer;
	LDBlockStatisticsHash::iterator LDBlockHashPointer = LDBlockStatisticsHashTable.begin();
	LDBlockCausalSNPHash::iterator LDBlockCausalHashPointer = LDBlockCausalSNPHashTable.begin();
	
	for (int i=0; i < numTotalPanels; i++) //for each LDBlock store the statistics
	{
		for (int alternate=0; alternate < numAlternatePerPanel; alternate++)
		{		
			causalIndex = candidateSNPIndices.at( (int)causalSNPPositions.at(alternateCount) );			
			fileHashPointer =  alternateGenlibFiles.find( causalIndex );
			if ( fileHashPointer != alternateEnd )
			{
				random_generator->generate_uniform(1, 0.0, (double) fileHashPointer->second->length(), &statisticReadLine);
				
				for (snpDataHash::iterator snpHashIt = snpDataHashTable.begin(); snpHashIt != snpDataHashTable.end(); snpHashIt++)
				{
					string rsid( snpHashIt->first );
					string snpType( tr1::get<SNP_TYPE>( snpHashIt->second ) );
					int index( tr1::get<INDEX>( snpHashIt->second ) );
					
					if ( snpType != NULLSNPLabel )
					{
						LDBlockHashPointer->second->insert( make_pair( rsid, strtod(fileHashPointer->second->at( (int)statisticReadLine)->at( index ).c_str(), NULL) ) );
						if ( index == causalIndex )
						{
							LDBlockCausalHashPointer->second->push_back( rsid ); // store the causal SNP for this LDBlock
						}
						
					}
				}
			}
			else 
			{
				cerr << "There is problem with alternate SNP, index is: " << causalIndex << endl;
			}
			alternateCount++;
			LDBlockHashPointer++;
			LDBlockCausalHashPointer++;
		}
		for (int null=0; null < numNullPerPanel; null++)
		{
			causalIndex = nullSNPIndices.at( (int)nullSNPPositions.at(nullCount) );
			fileHashPointer =  nullGenlibFiles.find( causalIndex );
			if ( fileHashPointer != nullEnd )
			{
				random_generator->generate_uniform(1, 0.0, (double) fileHashPointer->second->length(), &statisticReadLine);
				
				for (snpDataHash::iterator snpHashIt = snpDataHashTable.begin(); snpHashIt != snpDataHashTable.end(); snpHashIt++)
				{
					string rsid( snpHashIt->first );
					string snpType( tr1::get<SNP_TYPE>( snpHashIt->second ) );
					int index( tr1::get<INDEX>( snpHashIt->second ) );
					
					if ( snpType != NULLSNPLabel )
					{
						LDBlockHashPointer->second->insert( make_pair( rsid, strtod(fileHashPointer->second->at( (int)statisticReadLine)->at( index ).c_str(), NULL) ) );						
					}
				}
			}
			else 
			{
				cerr << "There is problem with null SNP, index is: " << causalIndex << endl;
			}
			
			nullCount++;
			LDBlockHashPointer++;
			LDBlockCausalHashPointer++;
		}
	}
	
	time(&timeEnd);
	timeDiff = difftime( timeEnd, timeStart);
	cout << "Finished. Time elapsed: " << timeDiff << " seconds." << endl;
	
	//CLEAR Dynamic DATA
	for ( tr1::unordered_map<int, InputFile*>::iterator it = alternateGenlibFiles.begin(); it != alternateGenlibFiles.end(); it++ )
	{
		if ( it->second )
		{
			delete it->second;	
		}
	}
	alternateGenlibFiles.clear();
	
	for ( tr1::unordered_map<int, InputFile*>::iterator it = nullGenlibFiles.begin(); it != nullGenlibFiles.end(); it++ )
	{
		if ( it->second )
		{
			delete it->second;	
		}
	}
	nullGenlibFiles.clear();
	
	delete random_generator;
}



