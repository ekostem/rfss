/*
 *  TraditionalSNPSelection.h
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 11/3/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#ifndef TRADITIONALSNPSELECTION_H_
#define TRADITIONALSNPSELECTION_H_


#include "AnalyzeGWAS.h"
#include "Utility.h"
#include <cmath>
#include <string>
#include <algorithm>
#include <tr1/unordered_map>



class CorrelationApproach : public AnalyzeGWAS
{
public:
	CorrelationApproach();
	~CorrelationApproach();
	
	void readData(const LDBlockPointerVector&,
								const LDBlockStatisticsHash&,
								const LDBlockCausalProbabilityHash&,
								const LDBlockCausalSNPHash&
								);
	
	void dumpResults( const string& );
	
private:
	double correlationThreshold;
	struct followUpSNP
	{
		double likelihood;
		double candidateStatistic;
		bool isCandidateCausal;
		bool isAlternate;
		int panelNumber;
	};
	
	struct sortSNPs 
	{
		bool operator()(const followUpSNP& lhs, const followUpSNP& rhs)
		{
			return lhs.likelihood > rhs.likelihood;
		}
	};
	
	vector<followUpSNP> local_followUpSNPs;
	
	
	
};


class DistanceApproach : public AnalyzeGWAS
{
public:
	DistanceApproach();
	~DistanceApproach();
	
	void readData(const LDBlockPointerVector&,
								const LDBlockStatisticsHash&,
								const LDBlockCausalProbabilityHash&,
								const LDBlockCausalSNPHash&
								);
	
	void dumpResults( const string& );
	
	
private:
	struct followUpSNP
	{
		double likelihood;
		double candidateStatistic;
		bool isCandidateCausal;
		bool isAlternate;
		int panelNumber;
	};
	
	struct sortSNPs 
	{
		bool operator()(const followUpSNP& lhs, const followUpSNP& rhs)
		{
			return lhs.likelihood > rhs.likelihood;
		}
	};
	
	vector<followUpSNP> local_followUpSNPs;
	
	
};


#endif



