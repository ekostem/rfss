/*
 *  ReadGWAS.cpp
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/25/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */


#include "../include/ReadGWAS.h"

ReadGWAS::ReadGWAS()
{
	
}




/******************************************************
 Below is a general format I call WTCCC format
 uses the dimTags many best tags and dimCandidates
 many best candidates in the multivariate analysis.
*********************************************************/

WTCCC::WTCCC() : ReadGWAS()
{
	local_population = NULL;
}


WTCCC::~WTCCC()
{
	gwasCandidateSNPs.clear();
	gwasTagSNPs.clear();
	gwasUserDefinedRegions.clear();
	skippedTagSNPs.clear();
	tagSNPHashTable.clear();
	candidateSNPHashTable.clear();
	local_population = NULL;
}


void WTCCC::registerPopulation( Population& pop)
{
	if (local_population)
	{
		local_population = NULL;
	}
	local_population = &pop;
}


void WTCCC::readData()
{
	getHashTableData();
}


void WTCCC::getHashTableData()
{

/************************************************************
rsID Statistic
Set of chromosomes spanned by the rsIDs = number of LDBlocks
read the chrom. information from the Population.
**************************************************************/
	if (!local_population)
	{
		cout << "Register Population" << endl;
		return;
	}
	
	InputFile* gwasFile = NULL;
	InputFile* gwasCandidateFile = NULL;
	gwasCandidateSNPs.clear();
	gwasTagSNPs.clear();
	gwasUserDefinedRegions.clear();
	skippedTagSNPs.clear();
	tagSNPHashTable.clear();
	candidateSNPHashTable.clear();
	
	try
	{
		gwasFile = new InputFile( gwasFilename );
		cout << "Reading GWAS file" << endl;
		for (long line=0; line < gwasFile->length(); line++)
		{
			string rsID( gwasFile->at(line)->at(0) );
			if (local_population->isSNPIncluded(rsID))
			{
				gwasTagSNPs.insert( rsID );
				tagSNPData t( strtod( gwasFile->at(line)->at(1).c_str(), NULL), // Statistic
											gwasFile->at(line)->at(2) // REGION
										);
				
				tagSNPHashTable.insert( make_pair( rsID, t) );
				gwasUserDefinedRegions.insert( gwasFile->at(line)->at(2) );
			}
			else 
			{
				skippedTagSNPs.insert( rsID );
			}
		}		
	}
	catch (InputFilexception& e)
	{
		cerr << e.what() << endl;
	}
	if (gwasFile)
	{
		cout << "Read: " << gwasTagSNPs.size() << " Tag SNPs" << endl;
		delete gwasFile;
	}
	/**************
	 * Set the number of LDBlocks
	 **************/
	numLDBlocks = gwasUserDefinedRegions.size();
	
	
	try
	{
		gwasCandidateFile = new InputFile( gwasCandidateSNPFilename );
		cout << "Reading candidate Filename" << endl;
		for (long line = 0; line < gwasCandidateFile->length(); line++)
		{
			string rsID( gwasCandidateFile->at(line)->at(0) );
			if ( local_population->isSNPIncluded(rsID) )
			{
				gwasCandidateSNPs.insert(rsID);
				
				candidateSNPData t( strtod( gwasCandidateFile->at(line)->at(1).c_str(), NULL), // Statistic
														gwasCandidateFile->at(line)->at(2) // REGION
													 );
				candidateSNPHashTable.insert( make_pair(rsID, t) );
			}
			else
			{
				skippedCandidateSNPs.insert( rsID );
			}
						
		}
	}
	catch (InputFilexception e)
	{
		cerr << e.what() << endl;
	}
	if (gwasCandidateFile)
	{
		cout << "Read: " << gwasCandidateSNPs.size() << " Candidate SNPs" << endl;
		delete gwasCandidateFile;
	}
}


void WTCCC::writeAllocation(int& value ) const
{
	value = numLDBlocks;
}

void WTCCC::writeData(LDBlockPointerVector& LDBlockVector,
											LDBlockStatisticsHash& LDBlockStatisticsHashTable,
											LDBlockCausalProbabilityHash& LDBlockCausalProbabilityHashTable,
											LDBlockCausalSNPHash& LDBlockCausalSNPHashTable
											)
{
	/******************************
	 * We assume writeAllocation is called
	 ******************************/
	
	tr1::unordered_map<string, int> regionToLDBlockIndex;
	int index = 0;
	for (set<string>::iterator it = gwasUserDefinedRegions.begin(); it != gwasUserDefinedRegions.end(); it++, index++)
	{
		regionToLDBlockIndex.insert( make_pair( *it, index ) );
	}
	
	tagSNPHash::iterator tagSNPHashPointer;
	candidateSNPHash::iterator candidateSNPHashPointer;
	
	LDBlockStatisticsHash::iterator LDBlockHashPointer;
	
	
	for ( tagSNPHashPointer = tagSNPHashTable.begin(); tagSNPHashPointer != tagSNPHashTable.end(); tagSNPHashPointer++)
	{
		string rsID = tagSNPHashPointer->first;
		string region = tr1::get<REGION>( tagSNPHashPointer->second );
		double statistic = tr1::get<STATISTIC>( tagSNPHashPointer->second );
		int LDno = regionToLDBlockIndex.find( region )->second;

		LDBlockHashPointer = LDBlockStatisticsHashTable.find( LDBlockVector[ LDno ] );
		LDBlockHashPointer->first->insertSNP( tagSNP(rsID) );
		LDBlockHashPointer->second->insert( make_pair( rsID, statistic) ); 
		
	}
	
	for (candidateSNPHashPointer = candidateSNPHashTable.begin(); candidateSNPHashPointer != candidateSNPHashTable.end(); candidateSNPHashPointer++)
	{
		
		string rsID = candidateSNPHashPointer->first;
		string region = tr1::get<REGION>( candidateSNPHashPointer->second );
		double statistic = tr1::get<STATISTIC>( candidateSNPHashPointer->second );
		int LDno = regionToLDBlockIndex.find( region )->second;
		
		LDBlockHashPointer = LDBlockStatisticsHashTable.find( LDBlockVector[ LDno ] );
		LDBlockHashPointer->first->insertSNP( candidateSNP(rsID) );
		LDBlockHashPointer->second->insert( make_pair( rsID, statistic) ); 
	}

}

























