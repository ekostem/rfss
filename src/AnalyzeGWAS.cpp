/*
 *  AnalyzeGWAS.cpp
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 10/28/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */


#include "../include/AnalyzeGWAS.h"



AnalyzeGWAS::AnalyzeGWAS()
{
	local_LDBlockPointerVector = NULL;
	local_LDBlockStatisticsHashTable = NULL;
	local_LDBlockCausalProbabilityHashTable = NULL;
	local_LDBlockCausalSNPHashTable = NULL;
	local_Population = NULL;
}

AnalyzeGWAS::~AnalyzeGWAS()
{
		local_LDBlockPointerVector = NULL;
		local_LDBlockStatisticsHashTable = NULL;
		local_LDBlockCausalProbabilityHashTable = NULL;
		local_LDBlockCausalSNPHashTable = NULL;
		local_Population = NULL;
}


void AnalyzeGWAS::readData(const LDBlockPointerVector& LDBlockVector,
													 const LDBlockStatisticsHash& LDBlockStatisticsHashTable,
													 const LDBlockCausalProbabilityHash& LDBlockCausalProbabilityHashTable,
													 const LDBlockCausalSNPHash& LDBlockCausalSNPHashTable
													 )
{	
	//attach pointers to data, they are created in the constructor
	if (! LDBlockVector.size() )
	{
		return;
	}
	local_LDBlockPointerVector = &LDBlockVector;
	local_LDBlockStatisticsHashTable = &LDBlockStatisticsHashTable;
	local_LDBlockCausalProbabilityHashTable = &LDBlockCausalProbabilityHashTable;
	local_LDBlockCausalSNPHashTable = &LDBlockCausalSNPHashTable; // here we will use this to test how well we do
}


void AnalyzeGWAS::usePopulation(const Population& pop)
{	
	//attach population to Analysis
	local_Population = &pop;
}


void AnalyzeGWAS::printMatrix( double* array, int nrow, int ncolstart, int ncolstop) const
{
	cout.precision(3);
	
	for (int i=0; i<nrow; i++)
	{
		for (int j=ncolstart; j< ncolstop; j++)
		{
			cout << array[ j*nrow + i] << ", ";			
		}
		cout << endl;
	}
}

void AnalyzeGWAS::getTagsFromLDBlock( const LDBlock* currentLD, vector<const SNP*>& tags)
{
	/*************************************
	 * Get const SNP* to the tagSNPs.
	 **************************************/	
	for (tagSet::const_iterator it = currentLD->getTagSet().begin(); it != currentLD->getTagSet().end(); it++)
	{
		tags.push_back( dynamic_cast<const SNP*>( &(*it) ) );
	}	
}

void AnalyzeGWAS::getCandidatesFromLDBlock( const LDBlock* currentLD, vector<const SNP*>& candidates)
{
	/*************************************
	 * Get const SNP* to the candidateSNPs.
	 **************************************/	
	for (candidateSet::const_iterator it = currentLD->getCandidateSet().begin(); it != currentLD->getCandidateSet().end(); it++)
	{
		candidates.push_back( dynamic_cast<const SNP*>( &(*it) ) );
	}	
}


const snpStatisticHash* AnalyzeGWAS::getStatisticHash( const LDBlock* currentLD )
{
	/*****************************************************
	 * Return the Statistics Hash Table of the LDBlock*
	 ****************************************************/		
	
	if ( !local_LDBlockStatisticsHashTable )
	{
		return NULL;
	}
	
	LDBlockStatisticsHash::const_iterator statisticHash_iterator;
	statisticHash_iterator = local_LDBlockStatisticsHashTable->find( const_cast<LDBlock*>(currentLD) );
	if ( statisticHash_iterator != local_LDBlockStatisticsHashTable->end() )
	{
		return statisticHash_iterator->second;	
	}
	else 
	{
		return NULL;
	}
}

void AnalyzeGWAS::filterTagSNPs( vector<const SNP*>& tags )
{
	/*********************************************
	 * Remove the tag SNPs that are in perfect LD
	 * with each other
	 ********************************************/
	
	if ( !local_Population )
	{
		return;			
	}
	vector<double> correlationMatrix;
	local_Population->getCorrelation(tags, tags, correlationMatrix);
	int dim = tags.size();
	vector<bool> mask( dim, true );
	
	for ( int tag1 = 0; tag1 < dim; tag1++)
	{
		if ( !mask[tag1] )
		{
			continue;
		}
		for (int tag2 = (tag1 + 1); tag2 < dim; tag2++)
		{
			if (  abs( correlationMatrix[ tag1*dim + tag2 ] ) > 0.95  || abs( correlationMatrix[ tag1*dim + tag2 ] ) < 0.05 )
			{
				mask[ tag2 ] = false;
			}
		}		
	}
	
	vector<const SNP*> temp;
	for (int i =0; i < dim; i++ )
	{
		if ( mask[i] )
		{
			temp.push_back( tags[i] );
		}
	}
	tags = temp;	
}

void AnalyzeGWAS::getBestTagSNPs( const SNP* candidate, const vector<const SNP*>& tags, vector<const SNP*>& bestTags, int n)
{
	/************************************************
	 * Get the tags 
	 * Sort them by correlation
	 * If perfect LD tag return only that
	 * Else get the best n that are filtered
	 ************************************************/
	if ( bestTags.size() )
	{
		bestTags.clear();
	}
	
	if ( tags.size() < n)
	{
		n = tags.size();
	}
	
	
	vector<double> correlationVector;
	vector<candidatetagSNPsHelper> candidatetagSNPsVector;
	
	local_Population->getCorrelation(candidate, tags, correlationVector);
	
	for (int i = 0; i < correlationVector.size(); i++)
	{
		candidatetagSNPsVector.push_back((candidatetagSNPsHelper)
																		{
																			correlationVector.at(i),
																			tags.at(i)
																		}
																		);
	}
	sort( candidatetagSNPsVector.begin(), candidatetagSNPsVector.end(), candidatetagSNPsHelperSort() );
	if ( abs( candidatetagSNPsVector.at(0).correlation ) > 0.98 ) // assumed perfect LD
	{
		bestTags.push_back( candidatetagSNPsVector.at(0).tagPtr );
		return;
	}


	int count = 0;
	int lastInsertPosition = 0;
	bool isInsertable = true;
	double tempCorrelation;
	while( count < n && lastInsertPosition < tags.size() )
	{
		if ( abs(candidatetagSNPsVector.at(lastInsertPosition).correlation) < 0.1 )
		{
			break;
		}
		
		
		isInsertable = true;
		for( int i=0; i < bestTags.size(); i++ )
		{
			local_Population->getCorrelation(bestTags.at(i),
																			 candidatetagSNPsVector.at(lastInsertPosition).tagPtr, 
																			 tempCorrelation
																			 );
			
			if ( abs(tempCorrelation) > 0.98 )
			{
				isInsertable = false;
				break;
			}
		}
		
		if (isInsertable)
		{
			bestTags.push_back( candidatetagSNPsVector.at(lastInsertPosition).tagPtr );
			count++;
		}
		lastInsertPosition++;
	}
	return;
}



void AnalyzeGWAS::getBestCandidateSNPs( const SNP* candidate, const vector<const SNP*>& candidates, vector<const SNP*>& bestCandidates, int n)
{
	if ( bestCandidates.size() )
	{
		bestCandidates.clear();	
	}
	
	if ( candidates.size() < n )
	{
		n = candidates.size();
	}
	
	
	if (n == 1)
	{
		bestCandidates.push_back( candidate );
		return;
	}
	
	
	vector<double> correlationVector;
	vector<candidatetagSNPsHelper> candidatetagSNPsVector;

	local_Population->getCorrelation(candidate, candidates, correlationVector);
	for (int i = 0; i < correlationVector.size(); i++)
	{
		candidatetagSNPsVector.push_back((candidatetagSNPsHelper)
																		{
																			correlationVector.at(i),
																			candidates.at(i)
																		}
																		);
	}
	sort( candidatetagSNPsVector.begin(), candidatetagSNPsVector.end(), candidatetagSNPsHelperSort() );
	for(int i=0; i < n; i++)
	{
		if ( abs( candidatetagSNPsVector.at(i).correlation) > 0.1 )
		{
			bestCandidates.push_back( candidatetagSNPsVector.at(i).tagPtr );
		}
		else 
		{
			break;
		}
	}
	return;
}




void AnalyzeGWAS::getStatistics(const snpStatisticHash* currentLDStatHash, vector<const SNP*>& snps, vector<double>& statistics)
{
	statistics.clear();
	if (!currentLDStatHash || !snps.size() )
	{
		return;
	}
	snpStatisticHash::const_iterator hash;
	for ( vector<const SNP*>::iterator it = snps.begin(); it != snps.end(); it++ )
	{
		hash = currentLDStatHash->find( (*it)->getrsID() );
		if (hash != currentLDStatHash->end() )
		{
			statistics.push_back( hash->second );
		}
		else 
		{
			cerr << "AnalyzeGWAS::getStatistics -> SNP is not in statistic hashtable" << endl;
			statistics.push_back( 0.0 );
		}
	}
}


