/*
 *  AnalyzeGWAS.h
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 10/28/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#ifndef ANALYZEGWAS_H_
#define ANALYZEGWAS_H_

#include "DataTypes.h"
#include "Population.h"
#include "Utility.h"
#include "LDBlock.h"

#include <cmath>
#include <ctime>
#include <string>
#include <algorithm>
#include <tr1/unordered_map>

using namespace std;


class Population;

class AnalyzeGWAS
{
public:
	
	AnalyzeGWAS();
	virtual ~AnalyzeGWAS();
	
	void readData(const LDBlockPointerVector&,
								const LDBlockStatisticsHash&,
								const LDBlockCausalProbabilityHash&,
								const LDBlockCausalSNPHash&
								); // Let's you access the GWAS data readonly

	void usePopulation(const Population& ); // set the population
	
	virtual void dumpResults( const string& ) = 0; // write results to file
	
	string getMethodName() const {return methodName;}
	void setMethodName( const string& name) {methodName = name;}
	
protected:
	string methodName;
		
	const LDBlockPointerVector* local_LDBlockPointerVector; // stores the pointers to the LDBlocks of a GWAS.
	const LDBlockStatisticsHash* local_LDBlockStatisticsHashTable; //LDBlock* => statistic hashTable {rsID, statistic}
	const LDBlockCausalProbabilityHash* local_LDBlockCausalProbabilityHashTable; // LDBlock* => causalProb hashTable {rsID, causalProb}
	const LDBlockCausalSNPHash* local_LDBlockCausalSNPHashTable; //LDBlock* => causal SNPs vector {rsIDS of the causal SNPs}
	const Population* local_Population;
	
	void getStatistics(const snpStatisticHash* currentLDStatHash, vector<const SNP*>& snps, vector<double>& statistics);
	void getTagsFromLDBlock( const LDBlock* currentLD, vector<const SNP*>& tags);
	void getCandidatesFromLDBlock( const LDBlock* currentLD, vector<const SNP*>& candidates);
	const snpStatisticHash* getStatisticHash( const LDBlock* currentLD);
	void filterTagSNPs( vector<const SNP*>& tags); //receives a vector of tags and filters collinear ones.
	void printMatrix( double* array, int nrow, int ncolstart, int ncolstop) const;
	
	void getBestTagSNPs( const SNP* candidate, const vector<const SNP*>& tags, vector<const SNP*>& bestTags, int n);
	void getBestCandidateSNPs( const SNP* candidate, const vector<const SNP*>& candidates, vector<const SNP*>& bestCandidates, int n);
	
	struct absComp
	{
		bool operator()(double i, double j) const { return abs(i) < abs(j); }
	};
	
	
	struct candidatetagSNPsHelper
	{
		double correlation;
		const SNP* tagPtr;
	};
	
	
	struct candidatetagSNPsHelperSort
	{
		bool operator()(const candidatetagSNPsHelper& lhs, const candidatetagSNPsHelper& rhs) const
		{
			return abs( lhs.correlation ) > abs( rhs.correlation );
		}
	};
	
	
	
};



#endif

