/*
 *  DataTypes.h
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 10/29/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#ifndef DATATYPES_H_
#define DATATYPES_H_

//#include "SNP.h"
//#include "LDBlock.h"

#include <set>
#include <vector>
#include <string>
#include <tr1/unordered_map>

using namespace std;

class SNP;
class tagSNP;
class candidateSNP;

class LDBlock;

/*
 These typedefs are used between classes. 
*/


typedef set<tagSNP> tagSet;
typedef set<candidateSNP> candidateSet;


typedef vector<LDBlock*> LDBlockPointerVector; // Store the LDBlock pointers of a GWAS
typedef tr1::unordered_map< string, double> snpStatisticHash; // Store the statistic of each SNP in a LDBlock
typedef tr1::unordered_map< LDBlock*, snpStatisticHash*> LDBlockStatisticsHash; // LDBLock to SNP statistics
typedef tr1::unordered_map< string, double> snpCausalProbabilityHash; // Probability of each SNP being causal in each LDBlock
typedef tr1::unordered_map< LDBlock*, snpCausalProbabilityHash*> LDBlockCausalProbabilityHash; // LDBlock to SNP causal probabilities
typedef vector<string> causalSNPrsIDVector;
typedef tr1::unordered_map< LDBlock*, causalSNPrsIDVector* > LDBlockCausalSNPHash; // LDBlock to vector of causal SNPs in the LDBlock


#endif

