/*
 *  TraditionalSNPSelection.h
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 11/3/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#ifndef TRADITIONALSNPSELECTION_H_
#define TRADITIONALSNPSELECTION_H_


#include "AnalyzeGWAS.h"
#include "Utility.h"
#include <string>
#include <algorithm>
#include <tr1/unordered_map>



class CorrelationApproach : public AnalyzeGWAS
{
public:
	CorrelationApproach();
	~CorrelationApproach();
	
	void readData(const LDBlockPointerVector&,
								const LDBlockStatisticsHash&,
								const LDBlockCausalProbabilityHash&,
								const LDBlockCausalSNPHash&
								);

	void setSignificanceLevel( double alpha );
	void dumpResults( const string& );
	void dumpResults2( const string& );
	
	void setCorrelationThreshold( double threshold) { correlationThreshold = threshold;}
	double getCorrelationThreshold() { return correlationThreshold;}
	void correlationSelection();
	
private:
	double correlationThreshold;
	double significanceLevel;
	double significanceThreshold;

	
	
	struct tagSNPHelper
	{
		double statistic;
		const SNP* tagPtr;
	};
	
	
	struct tagSNPHelperSort
	{
		bool operator()(const tagSNPHelper& lhs, const tagSNPHelper& rhs) const
		{
			return abs( lhs.statistic ) > abs( rhs.statistic );
		}
	};
	
	
	
	struct followUpSNP
	{
		double bestTagStatistic;
		double bestTagCorrelation;
		double candidateStatistic;
		bool isCandidateCausal;
		bool isAlternate;
		int panelNumber;
	};
	
	struct sortSNPs 
	{
		bool operator()(const followUpSNP& lhs, const followUpSNP& rhs)
		{
			if ( abs( abs( lhs.bestTagStatistic ) - abs( rhs.bestTagStatistic) ) < 1e-15 )
			{
				return abs( lhs.bestTagCorrelation ) > abs( rhs.bestTagCorrelation );
			}
			else 
			{
				return abs( lhs.bestTagStatistic ) > abs( rhs.bestTagStatistic );	
			}
		}
	};
	
	vector<followUpSNP> local_followUpSNPs;
	
	
	
};


class DistanceApproach : public AnalyzeGWAS
{
public:
	DistanceApproach();
	~DistanceApproach();
	
	void readData(const LDBlockPointerVector&,
								const LDBlockStatisticsHash&,
								const LDBlockCausalProbabilityHash&,
								const LDBlockCausalSNPHash&
								);
	
	void setSignificanceLevel( double alpha );
	void dumpResults( const string& );
	void dumpResults2( const string& );
	
	void setDistanceThreshold(long distance) { distanceThreshold = distance;}
	long getDistanceThreshold() { return distanceThreshold;}
	void distanceSelection();
	
	
private:
	long distanceThreshold;
	double significanceLevel;
	double significanceThreshold;

	
	
	struct tagSNPHelper
	{
		double statistic;
		long position;
		const SNP* tagPtr;
	};
	
	
	struct tagSNPHelperSort
	{
		bool operator()(const tagSNPHelper& lhs, const tagSNPHelper& rhs) const
		{
			return abs( lhs.statistic ) > abs( rhs.statistic );
		}
	};
	
	
	struct followUpSNP
	{
		double bestTagStatistic;
		long bestTagDistance;
		double candidateStatistic;
		bool isCandidateCausal;
		bool isAlternate;
		int panelNumber;
	};
	
	struct sortSNPs 
	{
		bool operator()(const followUpSNP& lhs, const followUpSNP& rhs)
		{
			if ( abs( abs( lhs.bestTagStatistic ) - abs( rhs.bestTagStatistic) ) < 1e-15 )
			{
				return abs( lhs.bestTagDistance ) < abs( rhs.bestTagDistance );
			}
			else 
			{
				return abs( lhs.bestTagStatistic ) > abs( rhs.bestTagStatistic );	
			}
		}
		
	};
	
	vector<followUpSNP> local_followUpSNPs;
	
	
};


#endif



