/*
 * Genotype.h
 *
 *  Created on: Oct 20, 2010
 *      Author: EMRAH KOSTEM
 */

#ifndef GWAS_H_
#define GWAS_H_

#include "SimulateGWAS.h"
#include "ReadGWAS.h"
#include "AnalyzeGWAS.h"
#include "LDBlock.h"

#include <string>
#include <vector>
#include <tr1/unordered_map>
using namespace std;


class GWAS
{
public:
	
	GWAS();
	~GWAS();

	void readData( ReadGWAS& ); // get the blocks from read wrapper (virtual)
	void readData( SimulateGWAS& ); // get the blocks from a simulation (virtual)
	
	void writeData( AnalyzeGWAS& ) const; // pass data structure to analysis method read only
	

	void readAllocation( ReadGWAS& );
	void readAllocation( SimulateGWAS& ); // read the space needed from the source
	
	const string& getInfo() const {return gwasInfo;}
	void setInfo(const string& info) { gwasInfo = info;}
	
	const string& getStudyName() const {return gwasName;}
	void setStudyName(const string& name) {gwasName = name;}
	
	
private:
	LDBlockPointerVector LDBlocks;
	LDBlockStatisticsHash LDBlockStatisticsHashTable;
	LDBlockCausalProbabilityHash LDBlockCausalProbabilityHashTable;
	LDBlockCausalSNPHash LDBlockCausalSNPHashTable;
	
	string gwasName;
	string gwasInfo;
	
	int numberofLDBlocks;
	void allocateSpace(); // Create the hash tables
	
};


#endif
