/*
 *  Population.h
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/21/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */

#ifndef POPULATION_H_
#define POPULATION_H_

#pragma warning(disable:981)
#pragma warning(disable:383)

#include "DataTypes.h"
#include "InputFile.h"
#include "SNP.h"
#include <string>
#include <cstdlib>
#include <vector>
#include <set>
#include <tr1/unordered_map>

using namespace std;

class SNP;

class Population {
public:
	Population();
	Population(const string& popName, const string& popCorrelationFilename, const string& popMAFFilename, const string& popMAPFilename); // creates data from specific formatted file
	Population(const Population&); // Copy constructor
	Population& operator=(const Population&); // assigment operator
	~Population();
	
	void getCorrelation(const string&, const string&, double&) const;
	void getCorrelation(const SNP* , const SNP* , double&) const;
	void getCorrelation(const SNP* , const vector<const SNP*>&, vector<double>&) const;
	void getCorrelation(const vector<const SNP*>&, const vector<const SNP*>&, vector<double>&) const;
	
	void setCorrelation(const string&, const string&, const double&);
	void setCorrelation(const SNP* , const SNP* , const double&);
	void setCorrelation(const SNP* , const vector<const SNP*>&, const vector<double>&);
	void setCorrelation(const vector<const SNP*>&, const vector<const SNP*>&, const vector<double>&);
	
	void getMAF(const string&, double&) const;
	void getMAF(const SNP*, double&) const;
	void getMAF(const vector<const SNP*>&, vector<double>&) const;
	
	void setMAF(const string&, const double&);
	void setMAF(const SNP*, const double&);
	void setMAF(const vector<const SNP*>&, const vector<double>&);
	
	
	void getPosition(const string&, long&) const;
	void getPosition(const SNP*, long&) const;
	void getPosition(const vector<const SNP*>&, vector<long>&) const;
	
	void setPosition(const string&, const long&);
	void setPosition(const SNP*, const long&);
	void setPosition(const vector<const SNP*>&, const vector<long>&);
	
	void setChromosome(const string& rsID, const string& chrom);
	void setChromosome(const SNP* snp, const string& chrom);
	void setChromosome(const vector<const SNP*>& snps, const vector<string>& chroms);
	
	void getChromosome(const string& rsID, string& chrom) const;
	void getChromosome(const SNP* snp, string& chrom) const;
	void getChromosome(const vector<const SNP*>& snps, vector<string>& chroms) const;
	
	const string& getName() const { return populationName; }
	const string& getInfo() const { return populationInfo; }
	
	void setName(const string& popName) { populationName = popName; } 
	void setInfo(const string& info) { populationInfo = info; }
	
	void dumpCorrelationHash();
	
	bool isSNPIncluded( const string& rsID);
	bool isSNPIncluded( const SNP* snp);
	
private:
	
	static const string emptyKey;
	static const string selfKey;
	
	typedef tr1::unordered_map<string, double> correlationHash;
	
	typedef tr1::unordered_map<string, double> MAFHash;
	
	typedef tr1::unordered_map<string, long> positionHash;
	
	typedef tr1::unordered_map<string, string> chromosomeHash;
	
	set< string > rsIDSet; // stores the rsIDs of the SNPs.
	
	correlationHash correlationHashTable;
	MAFHash MAFHashTable;
	positionHash positionHashTable;
	chromosomeHash chromosomeHashTable;
	
	string populationName;
	string populationInfo;
	
	string getCorrelationKey( const SNP* const, const SNP* const) const;
	string getCorrelationKey( const string&, const string&) const;
	
};


#endif

