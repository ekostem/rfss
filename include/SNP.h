/*
 *  SNP.h
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/21/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */

#ifndef SNP_H_
#define SNP_H_

#include "Population.h"

#include <string>
using namespace std;

class Population;

class SNP {
public:
	SNP() {rsID = string("");}
	SNP(const string&);
	SNP(const SNP&); //copy constructor
	SNP& operator=(const SNP&);
	bool operator==(const SNP&) const;
	bool operator<(const SNP& ) const;
	
	virtual ~SNP() {}
	
	const string& getrsID() const {return rsID;}
	void setrsID( const string& rsid){rsID.assign(rsid);}
	
	double getStatistic() const {return statistic;}
	void setStatistic(double stat) {statistic = stat;}

	long getPosition( Population& ) const;	
	double getMAF( Population&  ) const;	
	
protected:
	string rsID;
	double statistic;
	

};

class candidateSNP : public SNP {
public:
	candidateSNP() : SNP() {}
	candidateSNP(const string&);
	candidateSNP(const candidateSNP& ); //copy constructor
	candidateSNP& operator=(const candidateSNP&);
	bool operator==(const candidateSNP&) const;
	bool operator<(const candidateSNP& ) const;
	
	~candidateSNP();
	
private:
	
};

class tagSNP : public SNP {
public:
	tagSNP() : SNP() {}
	tagSNP(const string&);
	tagSNP(const tagSNP& ); //copy constructor
	tagSNP& operator=(const tagSNP&);
	bool operator==(const tagSNP&) const;
	bool operator<(const tagSNP& ) const;
	~tagSNP();
	
private:
	
};




#endif
