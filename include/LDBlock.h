/*
 *  LDBlock.h
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/22/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */

#ifndef LDBLOCK_H_
#define LDBLOCK_H_

#pragma warning(disable:383)

#include "DataTypes.h"
#include "SNP.h"
#include <set>
#include <string>
using namespace std;


class LDBlock 
{
public:
	
	LDBlock();
	LDBlock(const string&, const string& chromosome, unsigned long, unsigned long); // info, CHROM, start, stop
	LDBlock(const LDBlock&); // copy const.
	LDBlock& operator=(const LDBlock&); 
	~LDBlock();
		
	bool insertSNP( const tagSNP& );
	bool insertSNP( const candidateSNP& );

	bool removeSNP( const tagSNP& );
	bool removeSNP( const candidateSNP& );
	
	void setInfo( const string& str) { info.assign(str); }
	string getInfo() const {return info;}
	
	void setChromosome( string& chr ) { chromosome = chr;	}
	string getChromosome() const {return chromosome;}
	
	void setStartPosition( unsigned long start ) { startPosition = start;}
	unsigned long getStartPosition() const { return startPosition; } 
	
	void setStopPosition( unsigned long stop ) {stopPosition = stop;}
	unsigned long getStopPosition() const { return stopPosition;}
	
	const tagSet& getTagSet() const { return tagSNPs; }
	const candidateSet& getCandidateSet() const { return candidateSNPs; }
	
	int getTagNumber() const { return tagSNPs.size(); }
	int getCandidateNumber() const { return candidateSNPs.size(); }
									
private:
	
	tagSet tagSNPs;	
	candidateSet candidateSNPs;
	
	string info;
	string chromosome;
	unsigned long startPosition;
	unsigned long stopPosition;
};



#endif
