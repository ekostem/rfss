/*
 *  Utility.h
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/26/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#pragma warning(disable:981)

#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
using namespace std;


extern "C"
{
	// Get declaration for intel MKL
#include "math.h"
#include "mathimf.h"
#include "mkl.h"
#include "mkl_vml.h"
#include "mkl_vsl.h"
#include "mkl_vsl_functions.h"
#include "mkl_vml_functions.h"
#include "mkl_blas.h"
#include "mkl_lapack.h"
}


string itoa(int value, int base);

class RandomVariable
{
public:
	RandomVariable(const int seed, const int brng=VSL_BRNG_MT19937);
	~RandomVariable() {vslDeleteStream( &stream_);}
	void generate_normal(const int number_variables, const int mean, const int std, double* output, const int generate_method=VSL_METHOD_DGAUSSIAN_ICDF);
	void mvgenerate_normal(const int number_variables, int dimension, const double* mean, double* variance_covariance_matrix, double* output, const int generate_method=VSL_METHOD_DGAUSSIANMV_ICDF);
	void generate_uniform(const int number_variables, const double left, const double right, double* output, const int generate_method=VSL_METHOD_DUNIFORM_STD);
	
	
private:
	VSLStreamStatePtr stream_;
	int brng_;
	int seed_;
};


#endif
