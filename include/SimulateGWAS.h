/*
 *  SimulateGWAS.h
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/25/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */
#ifndef SimulateGWAS_H_
#define SimulateGWAS_H_

#include "InputFile.h"
#include "DataTypes.h"
#include "Utility.h"
#include "LDBlock.h"

#include <ctime>
#include <string>
#include <iostream>
#include <tr1/unordered_map>
#include <tr1/tuple>
using namespace std;


class SimulateGWAS
{
public:	
	
	SimulateGWAS();
	virtual ~SimulateGWAS() {}
	virtual void writeAllocation( int& value) const = 0;
	virtual void generateData() = 0;
	virtual void writeData(LDBlockPointerVector&,
												 LDBlockStatisticsHash&,
												 LDBlockCausalProbabilityHash&,
												 LDBlockCausalSNPHash&
												 ) = 0;
	
protected:
	
		
};


class GenlibSimulation : public SimulateGWAS
{
public:
	
	enum snpTupleEnum 
	{
		INDEX = 0, MAF, SNP_TYPE, POSITION		
	};
	// index of the SNP column in the simulation file.
	
	GenlibSimulation();
	~GenlibSimulation();
	
	void writeAllocation( int& value ) const; //passes the number of LDBlocks
	void generateData();
	void writeData(LDBlockPointerVector&,
								 LDBlockStatisticsHash&,
								 LDBlockCausalProbabilityHash&,
								 LDBlockCausalSNPHash&
								 );
	
	void setGenlibPath(const string& name) { genlibPathName = name;}
	void setMafPath(const string& name) { mafPathName = name;}
	void setMapPath(const string& name) { mapPathName = name;}
	void setPopulation(const string& name) {populationName = name;}
	void setRegion(const string& name) {regionName = name;}
	
	const string& getGenlibPath() const { return genlibPathName;}
	const string& getMafPath() const { return mafPathName;}
	const string& getMapPath() const { return mapPathName;}
	const string& getPopulation() const {return populationName;}
	const string& getRegion() const {return regionName;}
	
	void setRandomGeneratorSeed( int seed ) {randomGeneratorSeed = seed;}
	int getRandomGeneratorSeed() {return randomGeneratorSeed;}
	
	void setPanelNumber( int p ) {numTotalPanels = p;}
	void setAlternateNumber( int p) {numAlternatePerPanel = p;}
	void setNullNumber( int p ) {numNullPerPanel = p;}
	
	int getPanelNumber() const {return numTotalPanels;}
	int getAlternateNumber() const {return numAlternatePerPanel;}
	int getNullNumber() const {return numNullPerPanel;}
	
	
	
private:
	typedef tr1::tuple<int, double, string, long> snpDataTuple;
	typedef tr1::unordered_map<string, snpDataTuple > snpDataHash; // rsid to SNP info
	snpDataHash snpDataHashTable;
	
	vector<int> candidateSNPIndices;
	vector<int> nullSNPIndices;
	vector<int> tagSNPIndices;
	
	
	int numTotalPanels;
	int numAlternatePerPanel;
	int numNullPerPanel;
	
	int randomGeneratorSeed;
	
	string genlibPathName;
	string mafPathName;
	string mapPathName;
	string populationName;
	string regionName;
	
	string TAGSNPLabel;
	string CANDIDATESNPLabel;
	string NULLSNPLabel;
	
	void getHashTableData();
	
};



#endif
