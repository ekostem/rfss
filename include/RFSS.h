/*
 *  RFSS.h
 *  GWASLib_Xcode
 *
 *  Created by Emrah Kostem on 10/29/10.
 *  Copyright 2010 _EMRAH_KOSTEM_. All rights reserved.
 *
 */

#ifndef RFSS_H_
#define RFSS_H_

#include "AnalyzeGWAS.h"
#include "Utility.h"
#include <string>
#include <algorithm>
#include <tr1/unordered_map>

using namespace std;



class RFSS : public AnalyzeGWAS
{
public:
	RFSS();
	~RFSS();
	
	void readData(const LDBlockPointerVector&,
								const LDBlockStatisticsHash&,
								const LDBlockCausalProbabilityHash&,
								const LDBlockCausalSNPHash&
								);
	
	
	void dumpResults( const string& );
	void dumpResults2( const string& );
	
	void setCausalSNPProbability(double c) { causalSNPProbability = c; }
	void setSignificanceLevel( double alpha );
	void setCausalSNPPower( double p);

	double getSignificanceLevel() {return significanceThreshold;}
	double getcausalSNPNCP() {return causalSNPNCP;}
	
	void setTagCandidateDimension(int n, int m) {dimTags = n; dimCandidates = m;}
	
	void bestTagSelection();
	void allTagSelection();

	
private:
	
	int dimTags;
	int dimCandidates;
	
	double causalSNPProbability;
	double causalSNPPower;
	double significanceLevel;
	double significanceThreshold;
	double causalSNPNCP;
	
	double powerFunction(double mean);
	
	void computeLikelihood(vector<double>& candidateMAFs,
												 vector<double>& R, 
												 vector<double>& sHat, 
												 vector<double>& likelihood,
												 vector<double>& causalLikelihood,
												 vector<double>& noncausalLikelihood,
												 vector<double>& variance,
												 vector<double>& causalMean,
												 vector<double>& noncausalMean												 
												 );

	void computeLikelihoodMVN(vector<double>& candidateMAFs,
														vector<double>& Sigma_candidateSNPs, 
														vector<double>& Sigma_tagSNPs, 
														vector<double>& sHat_tags,
														vector<double>& R_candidateSNPs,
														vector<double>& likelihoods,
														vector<double>& causalLikelihood,
														vector<double>& noncausalLikelihood,
														vector<double>& variance,
														vector<double>& causalMean,
														vector<double>& noncausalMean
														);
	
	void computeLikelihoodMVN_single(vector<double>& candidateMAFs,
																	 vector<double>& Sigma_candidateSNPs, 
																	 vector<double>& Sigma_tagSNPs, 
																	 vector<double>& sHat_tags,
																	 vector<double>& R_candidateSNPs,
																	 double& likelihood,
																	 double& causalLikelihood,
																	 double& noncausalLikelihood,
																	 double& variance,
																	 double& causalMean,
																	 double& noncausalMean
																	 );

	void computeLikelihoodMVN_fast(vector<double>& candidateMAFs,
																 vector<double>& Sigma_candidateSNPs, 
																 vector<double>& Sigma_tagSNPs, 
																 vector<double>& sHat_tags,
																 vector<double>& R_candidateSNPs,
																 vector<double>& likelihoods,
																 vector<double>& causalLikelihood,
																 vector<double>& noncausalLikelihood,
																 vector<double>& variance,
																 vector<double>& causalMean,
																 vector<double>& noncausalMean
																 );
	
	
	void eigenCleaning( double* matrix, int dim);
	double detCholesky( double* matrix, int dim);
	double detLU( double* matrix, int dim);
	
	struct followUpSNP
	{
		double likelihood;
		double candidateStatistic;
		bool isCandidateCausal;
		bool isAlternate;
		int panelNumber;
		double causalLikelihood;
		double noncausalLikelihood;
		double variance;
		double causalMean;
		double noncausalMean;
		string rsID;
	};
	
	struct sortSNPs 
	{
		bool operator()(const followUpSNP& lhs, const followUpSNP& rhs)
		{
			return lhs.likelihood > rhs.likelihood;
		}
	};
	
	vector<followUpSNP> local_followUpSNPs;
	
	
};


#endif
