/*
 *  ReadGWAS.h
 *  GWASLIB
 *
 *  Created by Emrah Kostem on 10/25/10.
 *  Copyright 2010 Emrah Kostem. All rights reserved.
 *
 */

#ifndef ReadGWAS_H_
#define ReadGWAS_H_

#include "InputFile.h"
#include "DataTypes.h"
#include "Utility.h"
#include "LDBlock.h"

#include <string>
#include <iostream>
#include <tr1/unordered_map>
#include <tr1/tuple>

using namespace std;


class ReadGWAS
{
public:
	ReadGWAS();
	virtual ~ReadGWAS() {}
	virtual void writeAllocation(int& value ) const = 0; // number of LDBlocks
	virtual void readData() = 0;
	
	virtual void writeData(LDBlockPointerVector&,
												 LDBlockStatisticsHash&,
												 LDBlockCausalProbabilityHash&,
												 LDBlockCausalSNPHash&
												 ) = 0;

protected:
	
};

class WTCCC : public ReadGWAS
{
public:
	
	enum SNPDataEnum 
	{
		STATISTIC=0, REGION 
	};
	
	
	WTCCC();
	~WTCCC();
	
	void writeAllocation(int& value) const;

	void readData();

	void writeData(LDBlockPointerVector&,
								 LDBlockStatisticsHash&,
								 LDBlockCausalProbabilityHash&,
								 LDBlockCausalSNPHash&
								 );
	
	
	void setGwasFilename(string filename) {gwasFilename = filename;}
	string getGwasFilename() const {return gwasFilename;}
	
	void setGwasCandidateSNPFilename(string filename) {gwasCandidateSNPFilename = filename;}
	
	void registerPopulation( Population& pop);
	
private:
	
	string gwasFilename;
	string gwasCandidateSNPFilename;
	
	int numLDBlocks;
	
	
	typedef tr1::tuple<double, string> tagSNPData;
	typedef tr1::unordered_map<string, tagSNPData > tagSNPHash;
	tagSNPHash tagSNPHashTable;
	
	typedef tr1::tuple<double, string> candidateSNPData;
	typedef tr1::unordered_map<string, candidateSNPData> candidateSNPHash;
	candidateSNPHash candidateSNPHashTable;
	
	
	set<string> gwasCandidateSNPs;
	set<string> gwasTagSNPs;
	set<string> gwasUserDefinedRegions;
	
	set<string> skippedTagSNPs;
	set<string> skippedCandidateSNPs;
	
	Population* local_population;
	
	void getHashTableData();
	
};




#endif
